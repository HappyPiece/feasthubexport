﻿using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/cook")]
    [ApiController]
    [AuthorizeRole(FeastHubRoleType.Cook)]
    public class CookController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public CookController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet, Route("orders/available")]
        public async Task<IActionResult> GetAvailableOrders()
        {
            var cookId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetCookAvailableOrders(cookId);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/my")]
        public async Task<IActionResult> GetOrders([FromBody] FlatOrderQueryParameters query)
        {
            var cookId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetOrders(cookId, FeastHubRoleType.Cook, query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/take")]
        public async Task<IActionResult> TakeOrder([FromRoute] Guid orderId)
        {
            var cookId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.TakeToKitchen(orderId, cookId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/done")]
        public async Task<IActionResult> MarkOrderAsPackaged([FromRoute] Guid orderId)
        {
            var cookId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.PutForCourier(orderId, cookId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/cancel")]
        public async Task<IActionResult> CancelOrder([FromRoute] Guid orderId)
        {
            var cookId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.Cancel(orderId, cookId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
