﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/customer")]
    [ApiController]
    [Authorize]
    public class CustomerController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public CustomerController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost, Route("cart/edit")]
        public async Task<IActionResult> SetDishNumber(
            [FromQuery] Guid dishId,
            [FromQuery] int number
        )
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.SetDishNumberInCart(customerId, dishId, number);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpGet, Route("cart")]
        public async Task<IActionResult> GetCart()
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetCart(customerId);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/create")]
        public async Task<IActionResult> CreateOrder([FromBody] CreateOrderDTO dto)
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.CreateOrder(customerId, dto);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPut, Route("orders/{id}/cancel")]
        public async Task<IActionResult> CancelOrder([FromRoute] Guid id)
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.Cancel(id, customerId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/my")]
        public async Task<IActionResult> GetCustomerOrders(
            [FromBody] FlatOrderQueryParameters query
        )
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetOrders(
                customerId,
                FeastHubRoleType.Customer,
                query
            );
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{id}/copy")]
        public async Task<IActionResult> CopyOrderContent([FromRoute] Guid id)
        {
            var customerId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.CopyOrderContent(customerId, id);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
