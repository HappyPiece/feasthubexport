﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/notifications")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationProducerService _notificationProducer;

        public NotificationController(INotificationProducerService notificationProducer)
        {
            _notificationProducer = notificationProducer;
        }

        [HttpPost, Route("send/{userId}")]
        public async Task<IActionResult> Send([FromRoute] Guid userId, [FromBody] string message)
        {
            _notificationProducer.SendNotification(
                new NotificationDTO { AddresseeId = userId, Message = message }
            );
            return Ok();
        }
    }
}
