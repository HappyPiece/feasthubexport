﻿using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/courier")]
    [ApiController]
    [AuthorizeRole(FeastHubRoleType.Courier)]
    public class CourierController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public CourierController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet, Route("orders/available")]
        public async Task<IActionResult> GetAvailableOrders()
        {
            var courierId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetCourierAvailableOrders(courierId);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/my")]
        public async Task<IActionResult> GetOrders([FromBody] FlatOrderQueryParameters query)
        {
            var courierId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.GetOrders(courierId, FeastHubRoleType.Courier, query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/take")]
        public async Task<IActionResult> TakeOrder([FromRoute] Guid orderId)
        {
            var courierId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.TakeForDelivery(orderId, courierId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/done")]
        public async Task<IActionResult> MarkOrderAsPackaged([FromRoute] Guid orderId)
        {
            var courierId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.MarkAsDone(orderId, courierId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("orders/{orderId}/cancel")]
        public async Task<IActionResult> CancelOrder([FromRoute] Guid orderId)
        {
            var courierId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _orderService.Cancel(orderId, courierId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
