﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/rating")]
    [ApiController]
    [Authorize]
    public class RatingController : ControllerBase
    {
        private readonly IRatingService _ratingService;

        public RatingController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        [HttpPost, Route("dish/{dishId}/create")]
        public async Task<IActionResult> CreateRating([FromRoute] Guid dishId, [FromBody] int score)
        {
            var userId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _ratingService.CreateDishRating(
                dishId,
                userId,
                new CreateRatingDTO { Score = score }
            );
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPatch, Route("dish/{dishId}/edit")]
        public async Task<IActionResult> EditRating([FromRoute] Guid dishId, [FromBody] int score)
        {
            var userId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _ratingService.EditDishRating(
                dishId,
                userId,
                new EditRatingDTO { Score = score }
            );
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpDelete, Route("dish/{dishId}/delete")]
        public async Task<IActionResult> DeleteRating([FromRoute] Guid dishId)
        {
            var userId = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _ratingService.DeleteDishRating(dishId, userId);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
