﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers
{
    [Route("api/restaurant")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        [HttpGet]
        public async Task<IActionResult> GetRestaurantsPaged(
            [FromQuery] FlatRestaurantQueryParameters query
        )
        {
            var result = await _restaurantService.GetRestaurantsPaged(query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpGet, Route("{id}")]
        public async Task<IActionResult> GetRestaurant(Guid id)
        {
            var result = await _restaurantService.GetRestaurant(id);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("create")]
        public async Task<IActionResult> CreateRestaurant(CreateRestaurantDTO dto)
        {
            var result = await _restaurantService.CreateRestaurant(dto);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return StatusCode(201);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpGet, Route("{id}/dishes")]
        public async Task<IActionResult> GetRestaurantDishes(
            [FromRoute] Guid id,
            [FromQuery] FlatDishQueryParameters query
        )
        {
            var result = await _restaurantService.GetRestaurantDishes(id, query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
