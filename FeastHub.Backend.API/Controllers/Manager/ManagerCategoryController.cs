﻿using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers.Manager
{
    public partial class ManagerController : ControllerBase
    {
        [HttpPost, Route("restaurant/{restaurantId}/categories/create")]
        public async Task<IActionResult> CreateCategories([FromRoute] Guid restaurantId, [FromBody] List<string> categories)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.CreateCategories(restaurantId, categories);
            return result.Evaluate<IActionResult>(() => { return StatusCode(201); }, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpPatch, Route("restaurant/{restaurantId}/categories/edit")]
        public async Task<IActionResult> EditCategories([FromRoute] Guid restaurantId, [FromBody] Dictionary<string, string> categories)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.EditCategories(restaurantId, categories);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpDelete, Route("restaurant/{restaurantId}/categories/delete")]
        public async Task<IActionResult> DeleteCategories([FromRoute] Guid restaurantId, [FromBody] List<string> categories)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.DeleteCategories(restaurantId, categories);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }
    }
}
