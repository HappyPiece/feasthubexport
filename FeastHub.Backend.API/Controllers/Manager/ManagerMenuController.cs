﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers.Manager
{
    public partial class ManagerController : ControllerBase
    {
        [HttpPost, Route("restaurant/{restaurantId}/menu/create")]
        public async Task<IActionResult> CreateMenu([FromRoute] Guid restaurantId, [FromBody] CreateMenuDTO dto)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.CreateMenu(restaurantId, dto);
            return result.Evaluate<IActionResult>(() => { return StatusCode(201); }, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpPatch, Route("menu/{menuId}/edit")]
        public async Task<IActionResult> EditMenu([FromRoute] Guid menuId, [FromBody] EditMenuDTO dto)
        {
            var check = await _restaurantService.CanManageMenu(menuId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.EditMenu(menuId, dto);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpDelete, Route("menu/{menuId}/delete")]
        public async Task<IActionResult> DeleteMenu([FromRoute] Guid menuId)
        {
            var check = await _restaurantService.CanManageMenu(menuId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.DeleteMenu(menuId);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpPatch, Route("menu/{menuId}/dishes/add")]
        public async Task<IActionResult> AddMenuDishes([FromRoute] Guid menuId, [FromBody] List<Guid> dishIds)
        {
            var check = await _restaurantService.CanManageMenu(menuId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.AddDishesToMenu(menuId, dishIds);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpPatch, Route("menu/{menuId}/dishes/remove")]
        public async Task<IActionResult> RemoveMenuDishes([FromRoute] Guid menuId, [FromBody] List<Guid> dishIds)
        {
            var check = await _restaurantService.CanManageMenu(menuId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _restaurantService.RemoveDishesFromMenu(menuId, dishIds);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }
    }
}
