﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers.Manager
{
    public partial class ManagerController : ControllerBase
    {
        [HttpPost, Route("restaurant/{restaurantId}/dish/create")]
        public async Task<IActionResult> CreateDish([FromRoute] Guid restaurantId, [FromBody] CreateDishDTO dto)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _dishService.CreateDish(restaurantId, dto);
            return result.Evaluate<IActionResult>(() => { return StatusCode(201); }, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpPatch, Route("dish/{dishId}/edit")]
        public async Task<IActionResult> EditDish([FromRoute] Guid dishId, [FromBody] EditDishDTO dto)
        {
            var check = await _restaurantService.CanManageDish(dishId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _dishService.EditDish(dishId, dto);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }

        [HttpDelete, Route("dish/{dishId}/delete")]
        public async Task<IActionResult> DeleteDish([FromRoute] Guid dishId)
        {
            var check = await _restaurantService.CanManageDish(dishId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _dishService.DeleteDish(dishId);
            return result.Evaluate<IActionResult>(NoContent, OperationResultUtilities.DefaultEvaluator);
        }
    }
}
