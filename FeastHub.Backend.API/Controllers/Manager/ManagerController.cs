﻿using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Backend.API.Controllers.Manager
{
    [Route("api/manage")]
    [ApiController]
    [AuthorizeRole(FeastHubRoleType.Manager)]
    public partial class ManagerController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IDishService _dishService;
        private readonly IRestaurantService _restaurantService;
        public ManagerController(IOrderService orderService, IDishService dishService, IRestaurantService restaurantService)
        {
            _orderService = orderService;
            _dishService = dishService;
            _restaurantService = restaurantService;
        }

        [HttpPost, Route("restaurant/{restaurantId}/orders")]
        public async Task<IActionResult> GetRestaurantOrders([FromRoute] Guid restaurantId, [FromBody] OrderQueryParameters query)
        {
            var check = await _restaurantService.CanManage(restaurantId, GuidUtilities.ExtractGuidFromClaimsPrincipal(User));
            if (check.IsFailed) return StatusCode(403, check.Exception!.Message);
            var result = await _orderService.GetOrders(restaurantId, FeastHubRoleType.Manager, query);
            return result.Evaluate<IActionResult>(() => { return Ok(result.Value); }, OperationResultUtilities.DefaultEvaluator);
        }
    }
}
