﻿using FeastHub.Backend.BL;
using FeastHub.Backend.DAL;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Exceptions.ExceptionHandler;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.BindOptions<RabbitMQOptions>(builder.Configuration.GetRequiredSection("RabbitMQ"))
    .BindOptions<AppTokenOptions>(builder.Configuration.GetRequiredSection("Token"))
    .BindOptions<PagedViewOptions>(builder.Configuration.GetSection("PagedView"));

builder.Services.AddBackendBL(dbConnectionString: builder.Configuration.GetRequiredSection("BackendDb").Get<DbOptions>()!.Connection,
    options: builder.Configuration.GetRequiredSection("RabbitMQ").Get<RabbitMQOptions>()!);

builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition(FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme.Reference.Id,
        FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme);
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        { FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme, new List<string>() }
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

builder.Services.AddJwtAuthentication(builder.Configuration.GetRequiredSection("Token").Get<AppTokenOptions>()!);

builder.Services.AddHealthChecks();


var app = builder.Build();
await app.UseBackendBL();
app.UseExceptionInterceptorMiddleware();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapHealthChecks("/health");
app.MapControllers();
app.Run();