using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Exceptions.ExceptionHandler;
using FeastHub.Common.Utilities;
using FeastHub.Notifications.BL;
using FeastHub.Notifications.BL.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

var tokenOptions = builder.Configuration.GetRequiredSection("Token").Get<AppTokenOptions>()!;

builder.Services
    .BindOptions<RabbitMQOptions>(builder.Configuration.GetRequiredSection("RabbitMQ"))
    .BindOptions<AppTokenOptions>(builder.Configuration.GetRequiredSection("Token"));

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder =>
    {
        builder.AllowAnyMethod().AllowAnyHeader().AllowCredentials().SetIsOriginAllowed(_ => true);
    });
});
builder.Services.AddNotificationsBL(
    builder.Configuration.GetRequiredSection("RabbitMQ").Get<RabbitMQOptions>()!
);

builder.Services
    .AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition(
        FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme.Reference.Id,
        FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme
    );
    options.AddSecurityRequirement(
        new OpenApiSecurityRequirement
        {
            { FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme, new List<string>() }
        }
    );

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

builder.Services
    .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = false;
        options.TokenValidationParameters = TokenUtilities.GenerateDefaultTokenValidationParameters(
            tokenOptions
        );
        options.Events = new JwtBearerEvents
        {
            OnMessageReceived = (context) =>
            {
                var accessToken = context.Request.Query["access_token"];

                Console.WriteLine($"Access token: {accessToken}");
                var path = context.HttpContext.Request.Path;
                Console.WriteLine($"Path: {path}");

                if (!string.IsNullOrEmpty(accessToken) && (path.StartsWithSegments("/hub")))
                {
                    context.Token = accessToken;
                }
                return Task.CompletedTask;
            }
        };
    });

builder.Services.AddHealthChecks();

var app = builder.Build();

app.UseExceptionInterceptorMiddleware();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();
app.UseRouting();
app.UseCors();

app.UseAuthentication();
app.UseAuthorization();

app.MapHealthChecks("/health");
app.MapControllers();
app.MapHub<NotificationHub>("/hub");
app.Run();
