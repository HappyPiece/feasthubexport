using FeastHub.Common.DTO;

namespace FeastHub.Test.Unit
{
    public class RoleTest
    {
        [Fact]
        public void RootRole_CanOperateAny_ExceptForHimself()
        {
            var operatorRoles = new HashSet<FeastHubRoleType>() { FeastHubRoleType.Root };
            var objectRoles = new HashSet<FeastHubRoleType>();
            foreach (var role in Enum.GetValues(typeof(FeastHubRoleType)).Cast<FeastHubRoleType>())
            {
                if (role != FeastHubRoleType.Root) objectRoles.Add(role);
            }


            Assert.True(operatorRoles.CanOperate(objectRoles));
        }

        [Fact]
        public void RootRole_CannotBeAssigned()
        {
            Assert.Equal(new HashSet<FeastHubRoleType>(), FeastHubRoleType.Root.CanBeAssignedBy());
        }

        [Fact]
        public void AdministratorRole_CanBeAssignedByRoot()
        {
            HashSet<FeastHubRoleType> rootRole = new() { FeastHubRoleType.Root };

            var result = FeastHubRoleType.Administrator.CanBeAssignedBy();

            Assert.Equal(rootRole, result);
        }

        [Fact]
        public void CookRole_CanBeAssignedByRootAndAdministrator()
        {
            HashSet<FeastHubRoleType> rootAdminRole = new() { FeastHubRoleType.Root, FeastHubRoleType.Administrator };

            var result = FeastHubRoleType.Cook.CanBeAssignedBy();

            Assert.Equal(rootAdminRole, result.ToHashSet());
        }

        [Fact]
        public void RootRole_CanAssignEveryOther_ExceptItself()
        {
            var allButRoot = new HashSet<FeastHubRoleType>();
            foreach (var role in Enum.GetValues(typeof(FeastHubRoleType)).Cast<FeastHubRoleType>())
            {
                if (role != FeastHubRoleType.Root) allButRoot.Add(role);
            }
            var root = new HashSet<FeastHubRoleType>() { FeastHubRoleType.Root };


            Assert.Equal(allButRoot, root.CanAssign());
        }

        [Fact]
        public void AdminRole_CannotAssignAdmin()
        {
            HashSet<FeastHubRoleType> adminRoles = new() { FeastHubRoleType.Administrator, FeastHubRoleType.Cook },
                assignedRoles = new() { FeastHubRoleType.Manager, FeastHubRoleType.Administrator };

            Assert.False(adminRoles.CanAssign(assignedRoles));
        }

        [Fact]
        public void AdminRole_CanAssignCookAndManager()
        {
            HashSet<FeastHubRoleType> adminRoles = new() { FeastHubRoleType.Administrator, FeastHubRoleType.Cook },
                assignedRoles = new() { FeastHubRoleType.Manager, FeastHubRoleType.Cook };

            Assert.True(adminRoles.CanAssign(assignedRoles));
        }

        [Fact]
        public void AdministratorRole_CanAssignEveryOther_ExceptItselfAndRoot()
        {
            var allButRootAndAdministrator = new HashSet<FeastHubRoleType>();
            foreach (var role in Enum.GetValues(typeof(FeastHubRoleType)).Cast<FeastHubRoleType>())
            {
                allButRootAndAdministrator.Add(role);
            }
            allButRootAndAdministrator.ExceptWith(new HashSet<FeastHubRoleType>() { FeastHubRoleType.Administrator, FeastHubRoleType.Root });

            Assert.Equal(allButRootAndAdministrator, FeastHubRoleType.Administrator.CanAssign());
        }
    }
}