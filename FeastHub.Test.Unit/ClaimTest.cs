using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using System.Security.Claims;

namespace FeastHub.Test.Unit
{
    public class ClaimTest
    {
        private static ClaimsPrincipal GeneratePrincipalFromClaims(List<Claim> claims)
        {
            var identity = new ClaimsIdentity(claims, "Test", nameType: ClaimConfiguration.NameClaimType, roleType: ClaimConfiguration.RoleClaimType);
            return new ClaimsPrincipal(identity);
        }

        private static List<Claim> GenerateRoleClaims(List<FeastHubRoleType> roles)
        {
            var roleClaims = new List<Claim>();
            foreach (var role in roles)
            {
                roleClaims.Add(new Claim(ClaimConfiguration.RoleClaimType, role.ToString()));
            }
            return roleClaims;
        }

        [Fact]
        public void AllRolesAreExtractedFromPrincipal()
        {
            var roles = new List<FeastHubRoleType> {
                FeastHubRoleType.Manager, FeastHubRoleType.Customer, FeastHubRoleType.Root };
            var principal = GeneratePrincipalFromClaims(GenerateRoleClaims(roles));

            Assert.Equal(principal.ExtractRoles(), roles);
        }

        [Fact]
        public void OtherClaimTypesDoNotAffectResult()
        {
            var roles = new List<FeastHubRoleType> {
                FeastHubRoleType.Manager, FeastHubRoleType.Customer, FeastHubRoleType.Root };
            var claims = GenerateRoleClaims(roles);
            claims.Add(new Claim(ClaimConfiguration.NameClaimType, "Root"));
            var principal = GeneratePrincipalFromClaims(claims);
            

            Assert.Equal(principal.ExtractRoles(), roles);
        }


    }
}