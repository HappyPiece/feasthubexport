﻿using FeastHub.Backend.DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL
{
    public static class BackendDALExtension
    {
        public static IServiceCollection AddBackendDAL(this IServiceCollection services, string dbConnectionString)
        {
            if (dbConnectionString is null) throw new ArgumentNullException("Database connection string is null");
            services.AddDbContext<FeastHubBackendDbContext>(options =>
            {
                options.UseNpgsql(dbConnectionString);
            });
            return services;
        }

        public static async Task<WebApplication> UseBackendDAL(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var feastHubUserDbContext = scope.ServiceProvider
                    .GetRequiredService<FeastHubBackendDbContext>();
                if (feastHubUserDbContext.Database.GetPendingMigrations().Any())
                    await feastHubUserDbContext.Database.MigrateAsync();
            }

            return app;
        }
    }

    public static class BackendDALSoftDeleteExtension
    {
        public static IEnumerable<TSource> Undeleted<TSource>(this IEnumerable<TSource> queryable) where TSource : DbEntity
        {
            return queryable.Where(x => x.DeletedAt == null);
        }

        public static IEnumerable<TSource> Deleted<TSource>(this IEnumerable<TSource> queryable) where TSource : DbEntity
        {
            return queryable.Where(x => x.DeletedAt != null);
        }
    }
}
