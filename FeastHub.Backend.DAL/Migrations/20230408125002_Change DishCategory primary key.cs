﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FeastHub.Backend.DAL.Migrations
{
    /// <inheritdoc />
    public partial class ChangeDishCategoryprimarykey : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory");

            migrationBuilder.AddColumn<Guid>(
                name: "Id",
                table: "DishCategory",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "DishCategory");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory",
                column: "Name");
        }
    }
}
