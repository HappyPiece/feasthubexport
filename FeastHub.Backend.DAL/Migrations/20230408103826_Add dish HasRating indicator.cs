﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FeastHub.Backend.DAL.Migrations
{
    /// <inheritdoc />
    public partial class AdddishHasRatingindicator : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasRatings",
                table: "Dishes",
                type: "boolean",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasRatings",
                table: "Dishes");
        }
    }
}
