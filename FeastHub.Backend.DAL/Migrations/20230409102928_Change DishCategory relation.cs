﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FeastHub.Backend.DAL.Migrations
{
    /// <inheritdoc />
    public partial class ChangeDishCategoryrelation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carts_Order_OrderId",
                table: "Carts");

            migrationBuilder.DropForeignKey(
                name: "FK_DishCategory_Dishes_DishId",
                table: "DishCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_DishCategory_Restaurants_RestaurantId",
                table: "DishCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Order",
                table: "Order");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory");

            migrationBuilder.DropIndex(
                name: "IX_DishCategory_DishId",
                table: "DishCategory");

            migrationBuilder.DropColumn(
                name: "DishId",
                table: "DishCategory");

            migrationBuilder.RenameTable(
                name: "Order",
                newName: "Orders");

            migrationBuilder.RenameTable(
                name: "DishCategory",
                newName: "Categories");

            migrationBuilder.RenameIndex(
                name: "IX_DishCategory_RestaurantId",
                table: "Categories",
                newName: "IX_Categories_RestaurantId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Orders",
                table: "Orders",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Categories",
                table: "Categories",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "DishDishCategory",
                columns: table => new
                {
                    CategoriesId = table.Column<Guid>(type: "uuid", nullable: false),
                    DishId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DishDishCategory", x => new { x.CategoriesId, x.DishId });
                    table.ForeignKey(
                        name: "FK_DishDishCategory_Categories_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DishDishCategory_Dishes_DishId",
                        column: x => x.DishId,
                        principalTable: "Dishes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DishDishCategory_DishId",
                table: "DishDishCategory",
                column: "DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carts_Orders_OrderId",
                table: "Carts",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Restaurants_RestaurantId",
                table: "Categories",
                column: "RestaurantId",
                principalTable: "Restaurants",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carts_Orders_OrderId",
                table: "Carts");

            migrationBuilder.DropForeignKey(
                name: "FK_Categories_Restaurants_RestaurantId",
                table: "Categories");

            migrationBuilder.DropTable(
                name: "DishDishCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Orders",
                table: "Orders");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Categories",
                table: "Categories");

            migrationBuilder.RenameTable(
                name: "Orders",
                newName: "Order");

            migrationBuilder.RenameTable(
                name: "Categories",
                newName: "DishCategory");

            migrationBuilder.RenameIndex(
                name: "IX_Categories_RestaurantId",
                table: "DishCategory",
                newName: "IX_DishCategory_RestaurantId");

            migrationBuilder.AddColumn<Guid>(
                name: "DishId",
                table: "DishCategory",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Order",
                table: "Order",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DishCategory",
                table: "DishCategory",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_DishCategory_DishId",
                table: "DishCategory",
                column: "DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carts_Order_OrderId",
                table: "Carts",
                column: "OrderId",
                principalTable: "Order",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DishCategory_Dishes_DishId",
                table: "DishCategory",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DishCategory_Restaurants_RestaurantId",
                table: "DishCategory",
                column: "RestaurantId",
                principalTable: "Restaurants",
                principalColumn: "Id");
        }
    }
}
