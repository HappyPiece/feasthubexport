﻿using FeastHub.Common.DTO;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class Dish : DbEntity
    {
        public string Name { get; set; }
        public bool IsAccessible
        {
            get { return !IsDeleted && Menus.Where(x => x.IsActive).Any(); }
            private set { }
        }
        public double Price { get; set; }
        public string? Description { get; set; }
        public bool? IsVegeterian { get; set; }
        public string? PhotoUrl { get; set; }
        public List<Rating> Ratings { get; set; } = new();
        public double Rating
        {
            get { return Ratings.IsNullOrEmpty() ? 0 : Ratings.Average(x => x.Score); }
            private set { }
        }
        public bool HasRatings
        {
            get { return Ratings.IsNullOrEmpty() ? false : true; }
            private set { }
        }
        public Restaurant Restaurant { get; set; }
        public Guid RestaurantId { get; private set; }
        public List<Menu> Menus { get; set; } = new();

        public List<DishCategory> Categories { get; set; } = new();

        public static explicit operator DishDTO(Dish dish)
        {
            return new DishDTO
            {
                Id = dish.Id,
                Name = dish.Name,
                Description = dish.Description,
                Price = dish.Price,
                IsVegeterian = dish.IsVegeterian,
                PhotoUrl = dish.PhotoUrl,
                Categories = dish.Categories.Select(x => x.Name).ToList(),
                Menus = dish.Menus.Select(x => x.Id).ToList(),
                Rating = dish.Rating
            };
        }

        public class DishCategory
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
        }
    }
}
