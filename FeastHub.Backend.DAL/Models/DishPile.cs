﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class DishPile : DbEntity
    {
        public Dish Dish { get; set; }
        public int Count { get; set; }
        public double? ArchivePricePerUnit { get; set; } = null;

        public bool IsOrdered => ArchivePricePerUnit != null;
        public double PileTotal => (ArchivePricePerUnit ?? Dish.Price) * Count;
    }
}
