﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class User : DbEntity
    {
    }

    public class Customer : User
    {
        public Cart Cart { get; set; } = new();
        public List<Order> Orders { get; set; } = new();
    }
    public class Manager : User
    {
        public Restaurant? Restaurant { get; set; }
    }
    public class Courier : User
    {
        public List<Order> Orders { get; set; } = new();
    }
    public class Cook : User
    {
        public Restaurant? Restaurant { get; set; }
        public List<Order> Orders { get; set; } = new();
    }
}
