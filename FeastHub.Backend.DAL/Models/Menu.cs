﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class Menu : DbEntity
    {
        public string Name { get; set; } = "New Menu";
        public bool IsActive { get; set; } = false;
        public string? Description { get; set; }
        public string? PhotoUrl { get; set; }
        public List<Dish> Dishes { get; set; } = new();

        public Restaurant Restaurant { get; set; }
        public Guid RestaurantId { get; protected set; }

        public static explicit operator MenuDTO(Menu menu) => new ()
        {
            Id = menu.Id,
            Name = menu.Name,
            Description = menu.Description,
            Dishes = menu.Dishes.Undeleted().Select(x => (DishDTO)x).ToList(),
            PhotoUrl = menu.PhotoUrl,
            IsActive = menu.IsActive,
        };
    }
}
