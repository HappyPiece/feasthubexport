﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class Cart : DbEntity
    {
        public List<DishPile> Piles { get; set; } = new();
        public double CartTotal => Piles.Where(x => x.Dish.IsAccessible || x.IsOrdered).Sum(pile => pile.PileTotal);

        public Order? Order { get; set; }
        public Guid? OrderId { get; set; }

       

        public Guid? CustomerId { get; set; }

        public static explicit operator CartDTO(Cart cart)
        {
            var content = new Dictionary<Guid, int>();

            foreach (var pile in cart.Piles)
            {
                content.Add(pile.Dish.Id, pile.IsOrdered ? pile.Count : (pile.Dish.IsAccessible ? pile.Count : -1));
            }

            return new CartDTO
            {
                Id = cart.Id,
                Content = content,
                CartTotal = cart.CartTotal
            };
        }
    }
}
