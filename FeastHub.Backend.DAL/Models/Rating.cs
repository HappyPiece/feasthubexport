﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class Rating : DbEntity
    {
        public Dish Dish { get; set; }
        public Guid DishId { get; private set; }
        public int Score { get; set; }
        public Guid CustomerId { get; set; }
    }
}
