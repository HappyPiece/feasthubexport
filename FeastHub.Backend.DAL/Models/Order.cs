﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.DAL.Models
{
    public class Order : DbEntity
    {
        public DateTime? DeliveredAt { get; set; }
        public Cart Cart { get; set; }
        public string Address { get; set; }
        public double OrderTotal => Cart.CartTotal;
        public OrderStatus Status { get; set; }
        public Restaurant Restaurant { get; set; }
        public Guid CustomerId { get; set; }
        public Guid? CookId { get; set; }
        public Guid? CourierId { get; set; }
        public string Number
        {
            get
            {
                return Restaurant == null ? "WHAT" : $"{Restaurant.Id.ToString().Substring(0, 3)}" +
                    $"-{Restaurant.Orders.Count.ToString().PadLeft(2, '0')}" +
                    $"-{CreatedAt.Day.ToString().PadLeft(2, '0')}" +
                    $"-{CreatedAt.Month.ToString().PadLeft(2, '0')}";
            }
            private set { }
        }

        public static explicit operator OrderDTO(Order order)
        {
            return new OrderDTO
            {
                Address = order.Address,
                Cart = (CartDTO)order.Cart,
                CookId = order.CookId,
                CustomerId = order.CustomerId,
                CourierId = order.CourierId,
                Status = order.Status,
                DeliveredAt = order.DeliveredAt,
                Id = order.Id,
                RestaurantId = order.Restaurant.Id,
                Number = order.Number
            };
        }
    }
}
