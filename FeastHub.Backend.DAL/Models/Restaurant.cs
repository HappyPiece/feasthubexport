﻿using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FeastHub.Backend.DAL.Models.Dish;

namespace FeastHub.Backend.DAL.Models
{
    public class Restaurant : DbEntity
    {
        public string Name { get; set; }
        public List<Menu> Menus { get; set; } = new();
        public List<Dish> Dishes { get; set; } = new();
        public List<Guid> Cooks { get; set; } = new();
        public List<Guid> Managers { get; set; } = new();
        public List<Order> Orders { get; set; } = new();

        public double Rating
        {
            get
            {
                return Dishes.Where(x => x.HasRatings).IsNullOrEmpty() ? 0 : Dishes.Where(x => x.HasRatings).Average(x => x.Rating);
            }
            private set { _rating = value; }
        }

        private double? _rating = null;
        public List<DishCategory> Categories { get; set; } = new();

        public static explicit operator RestaurantDTO(Restaurant restaurant) => new()
        {
            Id = restaurant.Id,
            Name = restaurant.Name,
            Dishes = restaurant.Dishes.Undeleted().Select(x => (DishDTO)x).ToList(),
            Menus = restaurant.Menus.Select(x => (MenuDTO)x).ToList(),
            Cooks = restaurant.Cooks,
            Managers = restaurant.Managers,
            Categories = restaurant.Categories.Select(x => x.Name).ToList(),
            Rating = restaurant.Rating,
            Orders = restaurant.Orders.Select(x => (OrderDTO)x).ToList()
        };

        public static explicit operator RestaurantShortDTO(Restaurant restaurant) => new()
        {
            Id = restaurant.Id,
            Name = restaurant.Name,
            Rating = restaurant.Rating
        };
    }
}
