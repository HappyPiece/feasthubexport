﻿using FeastHub.Backend.DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using static FeastHub.Backend.DAL.Models.Dish;

namespace FeastHub.Backend.DAL
{
    public class FeastHubBackendDbContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<DishCategory> Categories { get; set; }


        public FeastHubBackendDbContext(DbContextOptions options) : base(options) { }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            foreach (var entry in this.ChangeTracker.Entries<Dish>())
            {
                if (entry.State == EntityState.Deleted)
                {
                    entry.Entity.DeletedAt = DateTime.UtcNow;
                    entry.State = EntityState.Modified;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<DishCategory>().HasKey(x => x.Id);

            builder.Entity<Dish>(dish =>
            {
                dish.Property(x => x.Rating).UsePropertyAccessMode(PropertyAccessMode.Property);
                dish.Property(x => x.HasRatings).UsePropertyAccessMode(PropertyAccessMode.Property);
                dish.Property(x => x.IsAccessible).UsePropertyAccessMode(PropertyAccessMode.Property);

                dish.HasMany(x => x.Categories).WithMany();
                dish.HasMany(x => x.Ratings).WithOne(x => x.Dish).OnDelete(DeleteBehavior.Cascade);
                dish.HasOne(x => x.Restaurant).WithMany(x => x.Dishes).HasForeignKey(x => x.RestaurantId);
            });

            builder.Entity<Restaurant>(restaurant =>
            {
                restaurant.Property(x => x.Rating).UsePropertyAccessMode(PropertyAccessMode.Property);
                restaurant.HasMany(x => x.Menus).WithOne(x => x.Restaurant).HasForeignKey(x => x.RestaurantId);
            });

            builder.Entity<Order>(order =>
            {
                order.Property(x => x.Number).UsePropertyAccessMode(PropertyAccessMode.Property);
            });

            builder.Entity<Cart>(cart =>
            {
                cart.HasOne(x => x.Order).WithOne(x => x.Cart).OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<DishPile>(pile =>
            {
                pile.HasOne(x => x.Dish).WithMany().OnDelete(DeleteBehavior.ClientSetNull);
            });

            builder.Entity<Rating>().HasAlternateKey(x => new { x.CustomerId, x.DishId });
        }
    }
}

/*
DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubBackendDb; User Id = postgres; Password = beebra228" \
dotnet ef migrations add "Add calculated dish rating" --project FeastHub.Backend.DAL --startup-project FeastHub.Backend.API

DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubBackendDb; User Id = postgres; Password = beebra228" \
dotnet ef migrations remove --project FeastHub.Backend.DAL --startup-project FeastHub.Backend.API

DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubBackendDb; User Id = postgres; Password = beebra228" \
dotnet ef database update --project FeastHub.Backend.DAL --startup-project FeastHub.Backend.API

DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubBackendDb; User Id = postgres; Password = beebra228" \
dotnet ef database drop --project FeastHub.Backend.DAL --startup-project FeastHub.Backend.API
 */