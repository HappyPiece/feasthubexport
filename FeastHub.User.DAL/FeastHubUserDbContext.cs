﻿using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Data;
using static FeastHub.User.DAL.Models.FeastHubUser;

namespace FeastHub.User.DAL
{
    public class FeastHubUserDbContext : IdentityDbContext<FeastHubUser, FeastHubRole, Guid, IdentityUserClaim<Guid>, FeastHubUserRole, IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public FeastHubUserDbContext(DbContextOptions<FeastHubUserDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<FeastHubUserRole>(userRole =>
            {
                userRole.HasOne(x => x.Role)
                    .WithMany(x => x.Roles)
                    .HasForeignKey(x => x.RoleId)
                    .OnDelete(DeleteBehavior.Cascade);
                userRole.HasOne(x => x.User)
                    .WithMany(x => x.Roles)
                    .HasForeignKey(x => x.UserId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<UserAssociate>().UseTphMappingStrategy();
        }

    }
    public class FeastHubUserStore 
        : UserStore<FeastHubUser, FeastHubRole, FeastHubUserDbContext, Guid, IdentityUserClaim<Guid>, FeastHubUserRole, IdentityUserLogin<Guid>, IdentityUserToken<Guid>, IdentityRoleClaim<Guid>>
    {
        public FeastHubUserStore(FeastHubUserDbContext context)
            : base(context)
        {
            AutoSaveChanges = false;
        }
    }
}

/*
DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubUserDb; User Id = postgres; Password = beebra228" \
dotnet ef migrations add "Add user type associations" --project FeastHub.User.DAL --startup-project FeastHub.User.API

DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubUserDb; User Id = postgres; Password = beebra228" \
dotnet ef migrations remove --project FeastHub.User.DAL --startup-project FeastHub.User.API

DB_CONNECTION_STRING="Server = localhost; Port = 5432; Database = FeastHubUserDb; User Id = postgres; Password = beebra228" \
dotnet ef database update --project FeastHub.User.DAL --startup-project FeastHub.User.API
 */
