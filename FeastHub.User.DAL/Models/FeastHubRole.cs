﻿using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.User.DAL.Models
{
    public class FeastHubRole : IdentityRole<Guid>
    {
        public FeastHubRoleType Type { get; set; }
        public ICollection<FeastHubUserRole> Roles { get; set; }

    }
}
