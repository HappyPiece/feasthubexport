﻿using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Identity;

namespace FeastHub.User.DAL.Models
{
    public class FeastHubUser : IdentityUser<Guid>
    {
        public string Name { get; set; }
        public DateTime? BirthDate { get; set; }
        public override string? UserName => base.Email;
        public Gender Gender { get; set; } = Gender.ApacheAttackHelicopter;
        public ICollection<FeastHubUserRole> Roles { get; set; } = new List<FeastHubUserRole>();

        public string? RefreshToken { get; set; } = null;
        public DateTime? RefreshTokenExpiryTime { get; set; } = null;

        public CustomerAssociate? CustomerSelf { get; set; }
        public ManagerAssociate? ManagerSelf { get; set; }
        public CookAssociate? CourierSelf { get; set; }
        public CourierAssociate? CookSelf { get; set; }

        public string? ResetToken { get; set; } = null;
        public string? ResetTokenId { get; set; } = null;
        public string? ResetTokenPurpose { get; set; } = null;
        public DateTime? ResetTokenExpiryTime { get; set; } = null;

        public DateTime? BannedAt { get; set; } = null;
        public bool IsBanned => BannedAt != null;

        public FeastHubUser() { }

        public FeastHubUser(CreateUserDTO dto)
        {
            Name = dto.Name;
            BirthDate = dto.BirthDate;
            Email = dto.Email;
            Gender = dto.Gender;
            PhoneNumber = dto.PhoneNumber;
        }

        public static explicit operator UserDTO(FeastHubUser user) =>
            new()
            {
                Id = user.Id,
                Name = user.Name,
                BirthDate = user.BirthDate,
                Email = user.Email!,
                Gender = user.Gender,
                PhoneNumber = user.PhoneNumber,
                Roles = user.Roles.Select(x => x.Role.Type).ToList(),
                BannedAt = user.BannedAt
            };

        public abstract class UserAssociate
        {
            public Guid Id { get; set; }
        }

        public class CustomerAssociate : UserAssociate
        {
            public string? Address { get; set; }
        }

        public class ManagerAssociate : UserAssociate { }

        public class CookAssociate : UserAssociate { }

        public class CourierAssociate : UserAssociate { }
    }
}
