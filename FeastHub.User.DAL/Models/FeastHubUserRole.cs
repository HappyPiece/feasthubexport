﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.User.DAL.Models
{
    public class FeastHubUserRole : IdentityUserRole<Guid>
    {
        public virtual FeastHubUser User { get; set; }
        public virtual FeastHubRole Role { get; set; }

    }
}
