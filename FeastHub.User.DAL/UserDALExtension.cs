﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.User.DAL
{
    public static class UserDALExtension
    {
        public static IServiceCollection AddUserDAL(this IServiceCollection services, string dbConnectionString)
        {
            services.AddDbContext<FeastHubUserDbContext>(options =>
            {
                options.UseNpgsql(dbConnectionString);
            });

            return services;
        }

        public async static Task<WebApplication> UseUserDAL(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var feastHubUserDbContext = scope.ServiceProvider
                    .GetRequiredService<FeastHubUserDbContext>();
                if (feastHubUserDbContext.Database.GetPendingMigrations().Any())
                    await feastHubUserDbContext.Database.MigrateAsync();
            }

            return app;
        }
    }
}
