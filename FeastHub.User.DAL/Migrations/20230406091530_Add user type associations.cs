﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FeastHub.User.DAL.Migrations
{
    /// <inheritdoc />
    public partial class Addusertypeassociations : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "CookSelfId",
                table: "AspNetUsers",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CourierSelfId",
                table: "AspNetUsers",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "CustomerSelfId",
                table: "AspNetUsers",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ManagerSelfId",
                table: "AspNetUsers",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserAssociate",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Discriminator = table.Column<string>(type: "text", nullable: false),
                    Address = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAssociate", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CookSelfId",
                table: "AspNetUsers",
                column: "CookSelfId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CourierSelfId",
                table: "AspNetUsers",
                column: "CourierSelfId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CustomerSelfId",
                table: "AspNetUsers",
                column: "CustomerSelfId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ManagerSelfId",
                table: "AspNetUsers",
                column: "ManagerSelfId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CookSelfId",
                table: "AspNetUsers",
                column: "CookSelfId",
                principalTable: "UserAssociate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CourierSelfId",
                table: "AspNetUsers",
                column: "CourierSelfId",
                principalTable: "UserAssociate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CustomerSelfId",
                table: "AspNetUsers",
                column: "CustomerSelfId",
                principalTable: "UserAssociate",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_UserAssociate_ManagerSelfId",
                table: "AspNetUsers",
                column: "ManagerSelfId",
                principalTable: "UserAssociate",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CookSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CourierSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_UserAssociate_CustomerSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_UserAssociate_ManagerSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "UserAssociate");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_CookSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_CourierSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_CustomerSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ManagerSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CookSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CourierSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CustomerSelfId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ManagerSelfId",
                table: "AspNetUsers");
        }
    }
}
