﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FeastHub.User.DAL.Migrations
{
    /// <inheritdoc />
    public partial class Addbantimecolumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "BannedAt",
                table: "AspNetUsers",
                type: "timestamp with time zone",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BannedAt",
                table: "AspNetUsers");
        }
    }
}
