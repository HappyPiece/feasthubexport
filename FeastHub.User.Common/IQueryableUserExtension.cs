﻿using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.User.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace FeastHub.User.BL.Common
{
    public static class IQueryableUserExtension
    {
        public static IOrderedQueryable<FeastHubUser> SortUsers(this IQueryable<FeastHubUser> queryable, UserSortingParameters parameters)
        {
            return parameters.SortingAttribute switch
            {
                UserSortingAttribute.Email => new Func<IQueryable<FeastHubUser>, IOrderedQueryable<FeastHubUser>>((users) =>
                {
                    return parameters?.SortingType is SortingType.Descending ? users.OrderByDescending(x => x.Email) : users.OrderBy(x => x.Email);
                })(queryable),

                _ => new Func<IQueryable<FeastHubUser>, IOrderedQueryable<FeastHubUser>>((users) =>
                {
                    return parameters?.SortingType is SortingType.Descending ? users.OrderByDescending(x => x.Name) : users.OrderBy(x => x.Name);
                })(queryable),
            };
        }

        public static IQueryable<FeastHubUser> FilterName(this IQueryable<FeastHubUser> queryable, string? name)
        {
            if (name == null)
            {
                return queryable;
            }

            return queryable.Where(x => EF.Functions.Like(x.Name.ToLower(), $"%{name.ToLower()}%"));
        }

        public static IQueryable<FeastHubUser> FilterEmail(this IQueryable<FeastHubUser> queryable, string? email)
        {
            if (email == null)
            {
                return queryable;
            }

            return queryable.Where(x => EF.Functions.Like(x.Email.ToLower(), $"%{email.ToLower()}%"));
        }

        public static IQueryable<FeastHubUser> FilterRoles(this IQueryable<FeastHubUser> queryable, List<FeastHubRoleType> roles)
        {
            foreach (var role in roles)
            {
                queryable = queryable.Where(x => x.Roles.Any(r => r.Role.Type == role));
            }

            return queryable;
            //return queryable.Where(x => roles.All(r => x.Roles.Any(y => y.Role.Type == r)));
        }
    }
}