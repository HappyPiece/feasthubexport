﻿using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FeastHub.User.BL.Common
{
    public class FeastHubUserManager : UserManager<FeastHubUser>
    {
        public FeastHubUserManager(IUserStore<FeastHubUser> store,
        IOptions<IdentityOptions> optionsAccessor,
        IPasswordHasher<FeastHubUser> passwordHasher,
        IEnumerable<IUserValidator<FeastHubUser>> userValidators,
        IEnumerable<IPasswordValidator<FeastHubUser>> passwordValidators,
        ILookupNormalizer keyNormalizer,
        IdentityErrorDescriber errors,
        IServiceProvider services,
        ILogger<UserManager<FeastHubUser>> logger) :
            base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            Store = store;
        }
    }
}
