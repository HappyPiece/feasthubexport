﻿using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.User.BL.Common
{
    public class FeastHubTokenProvider : IUserTwoFactorTokenProvider<FeastHubUser>
    {
        private const int KeySize = 32;
        private const int TokenLifetime = 1; // TODO: put this in config

        public FeastHubTokenProvider() { }

        public Task<bool> CanGenerateTwoFactorTokenAsync(
            UserManager<FeastHubUser> manager,
            FeastHubUser user
        )
        {
            return Task.FromResult(true);
        }

        public Task<string> GenerateAsync(
            string purpose,
            UserManager<FeastHubUser> manager,
            FeastHubUser user
        )
        {
            byte[] randomPayload = RandomNumberGenerator.GetBytes(KeySize),
                randomId = RandomNumberGenerator.GetBytes(KeySize);

            string id = Convert.ToBase64String(randomId),
                payload = Convert.ToBase64String(randomPayload);

            var token = $"{id}-{payload}";

            user.ResetTokenPurpose = purpose;
            user.ResetTokenExpiryTime = DateTime.UtcNow.AddDays(TokenLifetime);
            user.ResetTokenId = id;
            user.ResetToken = Convert.ToBase64String(SHA256.HashData(randomPayload));

            return Task.FromResult(token);
        }

        public Task<bool> ValidateAsync(
            string purpose,
            string token,
            UserManager<FeastHubUser> manager,
            FeastHubUser user
        )
        {
            var parsedToken = token.Split("-");
            if (parsedToken.Length != 2)
                return Task.FromResult(false);
            string id = parsedToken[0],
                payload = parsedToken[1];

            var payloadHash = Convert.ToBase64String(
                SHA256.HashData(Convert.FromBase64String(payload))
            );

            var result =
                user.ResetTokenId == id
                && payloadHash == user.ResetToken
                && user.ResetTokenPurpose == purpose
                && user.ResetTokenExpiryTime > DateTime.UtcNow;

            user.ResetTokenPurpose = null;
            user.ResetTokenExpiryTime = null;
            user.ResetTokenId = null;
            user.ResetToken = null;

            if (result)
            {
                user.RefreshToken = null;
                user.RefreshTokenExpiryTime = null;
            }

            return Task.FromResult(result);
        }
    }
}
