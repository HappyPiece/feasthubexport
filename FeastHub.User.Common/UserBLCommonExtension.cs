﻿using FeastHub.Common.DTO;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FeastHub.User.BL.Common
{
    public static class UserBLCommonExtension
    {
        public static async Task<WebApplication> EnsureRolesCreated(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<
                    RoleManager<FeastHubRole>
                >();
                var dbContext = scope.ServiceProvider.GetRequiredService<FeastHubUserDbContext>();

                foreach (
                    var roleType in Enum.GetValues(typeof(FeastHubRoleType))
                        .Cast<FeastHubRoleType>()
                )
                {
                    if (!await dbContext.Roles.Where(x => x.Type == roleType).AnyAsync())
                    {
                        var role = new FeastHubRole { Type = roleType, Name = roleType.ToString() };
                        await roleManager.CreateAsync(role);
                    }
                }

                await dbContext.SaveChangesAsync();
            }
            return app;
        }

        public static async Task<WebApplication> EnsureRootCreated(
            this WebApplication app,
            CreateUserDTO? rootOptions
        )
        {
            using (var scope = app.Services.CreateScope())
            {
                var roleManager = scope.ServiceProvider.GetRequiredService<
                    RoleManager<FeastHubRole>
                >();
                var userManager = scope.ServiceProvider.GetRequiredService<FeastHubUserManager>();
                var dbContext = scope.ServiceProvider.GetRequiredService<FeastHubUserDbContext>();

                if (rootOptions is not null)
                {
                    if (
                        !await dbContext.Users
                            .Where(x => x.Roles.Any(x => x.Role.Type == FeastHubRoleType.Root))
                            .AnyAsync()
                    )
                    {
                        var root = new FeastHubUser
                        {
                            Email = rootOptions.Email,
                            EmailConfirmed = true,
                            UserName = rootOptions.Name,
                            Name = rootOptions.Name
                        };
                        await userManager.CreateAsync(root, rootOptions.Password);
                        await userManager.AddToRoleAsync(root, FeastHubRoleType.Root.ToString());
                        dbContext.Entry(root).State = EntityState.Added;
                    }
                    else
                    {
                        var root = await dbContext.Users
                            .Where(x => x.Roles.Any(x => x.Role.Type == FeastHubRoleType.Root))
                            .FirstAsync();
                        var concurrencyStamp = root.ConcurrencyStamp;

                        var emailToken = await userManager.GenerateChangeEmailTokenAsync(
                            root,
                            rootOptions.Email
                        );
                        await userManager.ChangeEmailAsync(root, rootOptions.Email, emailToken);
                        var passwordToken = await userManager.GeneratePasswordResetTokenAsync(root);
                        root.ConcurrencyStamp = concurrencyStamp;
                        await userManager.ResetPasswordAsync(
                            root,
                            passwordToken,
                            rootOptions.Password
                        );
                    }

                    await dbContext.SaveChangesAsync();
                }
            }
            return app;
        }
    }
}
