﻿using FeastHub.Common.Configurations;
using FeastHub.User.DAL.Models;
using System.Security.Claims;

namespace FeastHub.User.BL.Common
{
    public static class UserClaimsExtension
    {
        public static List<Claim> GetClaims(this FeastHubUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimConfiguration.NameClaimType, user.Id.ToString()),
                new Claim(ClaimConfiguration.EmailClaimType, user.Email!)
            };

            foreach (var role in user.Roles)
            {
                claims.Add(new Claim(ClaimConfiguration.RoleClaimType, role.Role.Type.ToString()));
            }
            return claims;
        }
    }
}
