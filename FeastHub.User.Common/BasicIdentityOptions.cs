﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.User.BL.Common
{
    public class BasicIdentityOptions
    {
        public static readonly PasswordOptions PasswordOptions = new PasswordOptions
        {
            RequireDigit = true,
            RequireLowercase = false,
            RequireNonAlphanumeric = false,
            RequireUppercase = false,
            RequiredLength = 6,
            RequiredUniqueChars = 0
        };

        public static readonly UserOptions UserOptions = new UserOptions
        {
            RequireUniqueEmail = true
        };
    }
}
