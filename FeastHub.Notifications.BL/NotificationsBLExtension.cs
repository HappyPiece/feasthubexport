﻿using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Notifications.BL.Services;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace FeastHub.Notifications.BL
{
    public static class NotificationsBLExtension
    {
        public static IServiceCollection AddNotificationsBL(this IServiceCollection services, RabbitMQOptions options)
        {
            services.AddSingleton(service => new ConnectionFactory
            {
                HostName = options.HostName,
                UserName = options.UserName,
                Password = options.Password
            }.CreateConnection());
            services.AddHostedService<MessageReciever>();

            services.AddSignalR();
            services.AddSingleton<IUserIdProvider, NameUserIdProvider>();
            services.AddSingleton<INotificationService, NotificationService>();

            return services;
        }
    }
}
