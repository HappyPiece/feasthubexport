﻿using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;

namespace FeastHub.Notifications.BL.Services
{
    [Authorize]
    public class NotificationHub : Hub
    {
        public async Task SendNotification(NotificationDTO dto)
        {
            await Clients.User(dto.AddresseeId.ToString()).SendAsync("ReceiveMessage",
                                               $"{DateTime.UtcNow.ToString("s")} UTC: {dto.Message}");
        }
    }
}
