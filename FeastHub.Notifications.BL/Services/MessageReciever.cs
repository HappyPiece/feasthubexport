﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using System.Text.Json;
using FeastHub.Common.DTO;

namespace FeastHub.Notifications.BL.Services
{
    public class MessageReciever : BackgroundService
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly RabbitMQOptions _options;
        private readonly INotificationService _notificationService;

        public MessageReciever(IOptions<RabbitMQOptions> options, IConnection connection, INotificationService notificationService)
        {
            _notificationService = notificationService;
            _options = options.Value;
            _connection = connection;
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: _options.QueueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (ch, eventArgs) =>
            {
                var content = Encoding.UTF8.GetString(eventArgs.Body.ToArray());
                Console.WriteLine($"Получено сообщение: {content}");

                var notification = JsonSerializer.Deserialize<NotificationDTO>(content);
                await _notificationService.SendNotification(notification ?? throw new ArgumentNullException("Bad notification"));

                _channel.BasicAck(eventArgs.DeliveryTag, false);
            };

            _channel.BasicConsume(_options.QueueName, false, consumer);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
