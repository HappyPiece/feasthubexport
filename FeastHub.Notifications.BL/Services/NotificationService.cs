﻿using Microsoft.Extensions.Hosting;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using Microsoft.AspNetCore.SignalR;
using FeastHub.Common;

namespace FeastHub.Notifications.BL.Services
{
    public class NotificationService : INotificationService
    {
        private readonly IHubContext<NotificationHub> HubContext;
        public NotificationService(IHubContext<NotificationHub> hubContext)
        {
            HubContext = hubContext;
        }

        public async Task<OperationResult> SendNotification(NotificationDTO dto)
        {
            await HubContext.Clients.User(dto.AddresseeId.ToString()).SendAsync("ReceiveMessage",
                                           $"{DateTime.UtcNow.ToString("s")} UTC: {dto.Message}");

            return new OperationResult();
        }
    }
}
