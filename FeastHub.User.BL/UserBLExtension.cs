﻿using FeastHub.Common.Contracts;
using FeastHub.User.BL.Services;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using FeastHub.User.BL.Common;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using FeastHub.Common.DTO;

namespace FeastHub.User.BL
{
    public static class UserBLExtension
    {
        public static IServiceCollection AddUserBL(this IServiceCollection services, string dbConnectionString)
        {
            services.AddUserDAL(dbConnectionString);
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAuthService, AuthService>();

            services.AddIdentity<FeastHubUser, FeastHubRole>(options =>
            {

            }).AddEntityFrameworkStores<FeastHubUserDbContext>()
            .AddTokenProvider("Default", typeof(FeastHubTokenProvider))
            .AddUserManager<FeastHubUserManager>()
            .AddRoleManager<RoleManager<FeastHubRole>>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password = BasicIdentityOptions.PasswordOptions;
                options.User = BasicIdentityOptions.UserOptions;
            });

            return services;
        }
        public static async Task<WebApplication> UseUserBL(this WebApplication app, CreateUserDTO? rootOptions)
        {
            await app.UseUserDAL();

            await app.EnsureRolesCreated();
            await app.EnsureRootCreated(rootOptions);

            return app;
        }
    }
}
