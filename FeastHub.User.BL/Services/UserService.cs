﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Exceptions;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.User.BL.Services
{
    public class UserService : IUserService
    {
        private readonly FeastHubUserDbContext _dbContext;
        private readonly UserManager<FeastHubUser> _userManager;
        public UserService(FeastHubUserDbContext dbContext, UserManager<FeastHubUser> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public async Task<OperationResult<UserDTO>> GetProfile(Guid id)
        {
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(x => x.Role)
                .Where(x => x.Id == id).SingleOrDefaultAsync();
            if (user == null)
            {
                return new OperationResult<UserDTO>(new NotFoundException());
            }
            await _dbContext.Entry(user).Collection(x => x.Roles).LoadAsync();

            return new OperationResult<UserDTO>((UserDTO)user);
        }

        public async Task<OperationResult> EditProfile(Guid id, EditUserDTO dto)
        {
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(x => x.Role)
                .Where(x => x.Id == id).SingleOrDefaultAsync();
            if (user == null)
            {
                return new OperationResult<UserDTO>(new NotFoundException());
            }

            {
                user.BirthDate = dto.BirthDate ?? user.BirthDate;
                user.PhoneNumber = dto.PhoneNumber ?? user.PhoneNumber;
                user.Name = dto.Name ?? user.Name;
                user.Gender = dto.Gender ?? user.Gender;
            }

            await _dbContext.SaveChangesAsync();

            return new OperationResult<UserDTO>((UserDTO)user);
        }

        public async Task<OperationResult> ChangePassword(EditPasswordDTO dto)
        {
            var user = await _userManager.FindByEmailAsync(dto.Email);
            if (user == null)
            {
                return new OperationResult(new InvalidCredentialsException("Invalid email or password"));
            }
            var result = await _userManager.ChangePasswordAsync(user, dto.Password, dto.NewPassword);
            if (!result.Succeeded)
            {
                return new OperationResult(new IdentityException(result.Errors.ToList()));
            }
            
            return new OperationResult();
        }
    }
}