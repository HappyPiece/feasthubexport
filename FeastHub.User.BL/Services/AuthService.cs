﻿using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using FeastHub.User.BL.Common;
using Microsoft.Extensions.Options;

namespace FeastHub.User.BL.Services
{
    public class AuthService : IAuthService
    {
        private readonly FeastHubUserDbContext _dbContext;
        private readonly UserManager<FeastHubUser> _userManager;
        private readonly AppTokenOptions _tokenOptions;
        public AuthService(FeastHubUserDbContext dbContext,
            UserManager<FeastHubUser> userManager,
            IOptions<AppTokenOptions> tokenOptions)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _tokenOptions = tokenOptions.Value;
        }
        public async Task<OperationResult<TokenPairDTO>> Login(LoginDTO dto)
        {
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(x => x.Role)
                .Where(x => x.Email == dto.Email).SingleOrDefaultAsync();
            if (user == null || !(await _userManager.CheckPasswordAsync(user, dto.Password)))
            {
                return new OperationResult<TokenPairDTO>(new InvalidCredentialsException("Invalid email or password"));
            }
            if (user.IsBanned)
            {
                return new OperationResult<TokenPairDTO>(new AccessDeniedException("Your account has been suspended. " +
                    "If you think this is a mistake, please contact support"));
            }

            var pair = TokenUtilities.GenerateTokenPair(user.GetClaims(), _tokenOptions);

            SetRefreshToken(user, pair.RefreshToken, _tokenOptions);

            await _dbContext.SaveChangesAsync();

            return new OperationResult<TokenPairDTO>(pair);
        }

        public async Task<OperationResult> Logout(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return new OperationResult(new NotFoundException($"Unable to logout: it seems the user '{id}' has ceased to exist"));
            }

            user.RefreshToken = null;
            user.RefreshTokenExpiryTime = null;

            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult<TokenPairDTO>> Register(CreateUserDTO dto)
        {
            var user = new FeastHubUser(dto);
            var result = await _userManager.CreateAsync(user, dto.Password);

            if (!result.Succeeded)
            {
                return new OperationResult<TokenPairDTO>(new IdentityException(result.Errors.ToList()));
            }
            await _userManager.AddToRoleAsync(user, FeastHubRoleType.Customer.ToString());
            _dbContext.SaveChanges();

            return await Login(new LoginDTO
            {
                Email = dto.Email,
                Password = dto.Password
            });
        }

        public async Task<OperationResult<TokenPairDTO>> Refresh(TokenPairDTO dto)
        {
            var principal = TokenUtilities.GetPrincipalFromToken(dto.AccessToken, _tokenOptions);
            var userId = principal.Identity?.Name ?? throw new ArgumentNullException("Expected Identity.Name to be present in token principal");
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return new OperationResult<TokenPairDTO>(new NotFoundException($"Unable to refresh: it seems the user '{userId}' ceased to exist"));
            }

            if (user.IsBanned)
            {
                return new OperationResult<TokenPairDTO>(new AccessDeniedException("Your account has been suspended." +
                    "If you think this is a mistake, please contact support"));
            }

            if (user.RefreshToken != dto.RefreshToken || user.RefreshTokenExpiryTime < DateTime.UtcNow)
            {
                return new OperationResult<TokenPairDTO>(new BadTokenException("Token is wrong or expired"));
            }

            var pair = TokenUtilities.GenerateTokenPair(user.GetClaims(), _tokenOptions);
            SetRefreshToken(user, pair.RefreshToken, _tokenOptions);
            await _dbContext.SaveChangesAsync();

            return new OperationResult<TokenPairDTO>(pair);
        }
        private static void SetRefreshToken(FeastHubUser user, string refreshToken, AppTokenOptions tokenOptions)
        {
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddRefreshTokenLifetime(tokenOptions);
        }
    }
}
