﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Configurations
{
    public class DbOptions
    {
        [Required]
        public string Connection { get; set; }
    }
}
