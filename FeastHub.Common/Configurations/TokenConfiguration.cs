﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Configurations
{
    public class AppTokenOptions
    {
        [Required]
        public string Issuer { get; set; }
        [Required]
        public string Audience { get; set; }
        [Required]
        public string SecretKey { get; set; }

        public int AccessTokenLifetime { get; set; } = 30; // minutes
        public int RefreshTokenLifetime { get; set; } = 7; // days
    }
}
