﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Configurations
{
    public static class ClaimConfiguration
    {
        public const string RoleClaimType = ClaimTypes.Role;
        public const string NameClaimType = ClaimTypes.Name;
        public const string EmailClaimType = ClaimTypes.Email;
    }
}
