﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Configurations
{
    public static class OptionsExtension
    {
        public static IServiceCollection BindOptions<TOptions>(this IServiceCollection services, IConfigurationSection configuration) where TOptions : class
        {
            services.AddOptions<TOptions>()
                .Bind(configuration)
                .ValidateDataAnnotations()
                .ValidateOnStart();
            return services;
        }
    }
}
