﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Configurations
{
    public class PagedViewOptions
    {
        public int RestaurantPageSize { get; set; } = 3;
        public int MenuPageSize { get; set; } = 3;
        public int DishPageSize { get; set; } = 3;
        public int OrderPageSize { get; set; } = 3;
        public int UserPageSize { get; set; } = 3;
    }
}
