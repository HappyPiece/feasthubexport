﻿using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Security.Claims;

namespace FeastHub.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class AuthorizeRole : TypeFilterAttribute
    {
        public AuthorizeRole(IEnumerable<FeastHubRoleType> roles, bool verbatim = false) : base(typeof(AuthorizeRoleFilter))
        {
            Arguments = new object[] { roles, verbatim };
        }

        public AuthorizeRole(FeastHubRoleType role, bool verbatim = false)
            : this(new List<FeastHubRoleType>() { role }, verbatim) { }
    }

    public class AuthorizeRoleFilter : IAuthorizationFilter
    {
        private readonly IEnumerable<FeastHubRoleType> _roles;
        private readonly bool _verbatim;

        public AuthorizeRoleFilter(IEnumerable<FeastHubRoleType> roles, bool verbatim)
        {
            _roles = roles;
            _verbatim = verbatim;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.Identity == null || !context.HttpContext.User.Identity.IsAuthenticated)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            var userRoles = context.HttpContext.User.ExtractRoles().ToHashSet();
            if (!_verbatim) userRoles.UnionWith(userRoles.CanAssign());
            if (!userRoles.Union(_roles.ToHashSet()).Any())
            {
                context.Result = new ForbidResult();
                return;
            }
        }
    }
}
