﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateDishDTO
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public string? Description { get; set; }
        public bool? IsVegeterian { get; set; }
        public string? PhotoUrl { get; set; }
        public List<string> Categories { get; set; } = new();
    }

    public class DishDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string? Description { get; set; }
        public bool? IsVegeterian { get; set; }
        public string? PhotoUrl { get; set; }
        public List<string> Categories { get; set; } = new();
        public List<Guid> Menus { get; set; } = new();
        public double Rating { get; set; }
    }

    public class EditDishDTO
    {
        public string? Name { get; set; }
        public double? Price { get; set; }
        public string? Description { get; set; }
        public bool? IsVegeterian { get; set; }
        public string? PhotoUrl { get; set; }
        public List<string> Categories { get; set; } = new();
    }
}
