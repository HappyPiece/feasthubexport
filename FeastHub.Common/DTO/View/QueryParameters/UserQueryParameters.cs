﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class FlatUserQueryParameters : QueryParameters
    {
        [Display(Name = "Имя")]
        public string? Name { get; set; } = null;

        [Display(Name = "Email")]
        public string? Email { get; set; } = null;

        [Display(Name = "Роли")]
        public List<FeastHubRoleType> Roles { get; set; } = new List<FeastHubRoleType>();

        public SortingType SortingType { get; set; } = SortingType.Descending;

        public UserSortingAttribute SortingAttribute { get; set; } = UserSortingAttribute.Email;

        public static implicit operator UserQueryParameters(FlatUserQueryParameters queryParams)
        {
            return new UserQueryParameters
            {
                Email = queryParams.Email,
                Name = queryParams.Name,
                Roles = queryParams.Roles,
                Page = queryParams.Page,
                SortingParameters = new UserSortingParameters
                {
                    SortingAttribute = queryParams.SortingAttribute,
                    SortingType = queryParams.SortingType
                }
            };
        }
    }

    public class UserQueryParameters : QueryParameters
    {
        [Display(Name = "Имя")]
        public string? Name { get; set; } = null;

        [Display(Name = "Email")]
        public string? Email { get; set; } = null;

        [Display(Name = "Роли")]
        public List<FeastHubRoleType> Roles { get; set; } = new List<FeastHubRoleType>();
        public UserSortingParameters SortingParameters { get; set; } = new UserSortingParameters();
    }

    public class UserSortingParameters
    {
        public SortingType SortingType { get; set; } = SortingType.Descending;

        public UserSortingAttribute SortingAttribute { get; set; } = UserSortingAttribute.Email;
    }

    public enum UserSortingAttribute
    {
        [Display(Name = "Имя")]
        Name,

        [Display(Name = "Email")]
        Email
    }
}
