﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class FlatMenuQueryParameters : QueryParameters
    {
        public SortingType SortingType { get; set; }
        public MenuSortingAttribute SortingAttribute { get; set; }

        public static implicit operator MenuQueryParameters(FlatMenuQueryParameters queryParameters)
        {
            return new MenuQueryParameters
            {
                Page = queryParameters.Page,
                SortingParameters = new MenuSortingParameters
                {
                    SortingAttribute = queryParameters.SortingAttribute,
                    SortingType = queryParameters.SortingType
                }
            };
        }
    }

    public class MenuQueryParameters : QueryParameters
    {
        public MenuSortingParameters? SortingParameters { get; set; }
    }

    public class MenuSortingParameters
    {
        public SortingType SortingType { get; set; }
        public MenuSortingAttribute SortingAttribute { get; set; }
    }

    public enum MenuSortingAttribute
    {
        Name,
        Rating
    }
}
