﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class QueryParameters
    {
        public int Page { get; set; } = 1;
    }

    public enum SortingType
    {
        [Display(Name = "По возрастанию")]
        Ascending = 0,
        [Display(Name = "По убыванию")]
        Descending = 1
    }

    public enum ActionType
    {
        Add,
        Remove
    }
}
