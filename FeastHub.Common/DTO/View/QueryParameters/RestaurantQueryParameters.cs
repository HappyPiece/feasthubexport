﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class FlatRestaurantQueryParameters : QueryParameters
    {
        public string? Name { get; set; } = "";
        public SortingType SortingType { get; set; } = SortingType.Descending;
        public RestaurantSortingAttribute SortingAttribute { get; set; } =
            RestaurantSortingAttribute.Name;

        public static implicit operator RestaurantQueryParameters(
            FlatRestaurantQueryParameters queryParams
        )
        {
            return new RestaurantQueryParameters
            {
                Name = queryParams.Name,
                SortingParameters = new RestaurantSortingParameters
                {
                    SortingAttribute = queryParams.SortingAttribute,
                    SortingType = queryParams.SortingType
                },
                Page = queryParams.Page
            };
        }
    }

    public class RestaurantQueryParameters : QueryParameters
    {
        public string? Name { get; set; } = "";
        public RestaurantSortingParameters? SortingParameters { get; set; } =
            new RestaurantSortingParameters();
    }

    public class RestaurantSortingParameters
    {
        public SortingType SortingType { get; set; } = SortingType.Descending;
        public RestaurantSortingAttribute SortingAttribute { get; set; } =
            RestaurantSortingAttribute.Name;
    }

    public enum RestaurantSortingAttribute
    {
        [Display(Name = "Название")]
        Name,

        [Display(Name = "Рейтинг")]
        Rating
    }
}
