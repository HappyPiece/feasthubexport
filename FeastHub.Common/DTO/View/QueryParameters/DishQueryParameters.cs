﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class FlatDishQueryParameters : QueryParameters
    {
        public SortingType SortingType { get; set; }
        public DishSortingAttribute SortingAttribute { get; set; }
        public List<string> Categories { get; set; } = new();
        public List<Guid> MenuIds { get; set; } = new();
        public bool? IsVegeterian { get; set; }

        public static implicit operator DishQueryParameters(FlatDishQueryParameters queryParameters)
        {
            return new DishQueryParameters
            {
                Page = queryParameters.Page,
                SortingParameters = new DishSortingParameters
                {
                    SortingAttribute = queryParameters.SortingAttribute,
                    SortingType = queryParameters.SortingType
                },
                Categories = queryParameters.Categories,
                MenuIds = queryParameters.MenuIds,
                IsVegeterian = queryParameters.IsVegeterian
            };
        }
    }

    public class DishQueryParameters : QueryParameters
    {
        public DishSortingParameters? SortingParameters { get; set; }
        public List<string> Categories { get; set; } = new();
        public List<Guid> MenuIds { get; set; } = new();
        public bool? IsVegeterian { get; set; }
    }

    public class DishSortingParameters
    {
        public SortingType SortingType { get; set; }
        public DishSortingAttribute SortingAttribute { get; set; }
    }

    public enum DishSortingAttribute
    {
        Name,
        Price,
        Rating
    }
}
