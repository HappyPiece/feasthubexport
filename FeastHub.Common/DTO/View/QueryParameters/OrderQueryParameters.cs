﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View.QueryParameters
{
    public class FlatOrderQueryParameters : QueryParameters
    {
        public DateTime? StartOrderDate { get; set; }
        public DateTime? EndOrderDate { get; set; }
        public string? OrderNumber { get; set; }
        public SortingType SortingType { get; set; } = SortingType.Descending;
        public OrderSortingAttribute SortingAttribute { get; set; } =
            OrderSortingAttribute.CreationDate;

        public static implicit operator OrderQueryParameters(
            FlatOrderQueryParameters queryParameters
        )
        {
            return new OrderQueryParameters
            {
                EndOrderDate = queryParameters.EndOrderDate,
                OrderNumber = queryParameters.OrderNumber,
                Page = queryParameters.Page,
                SortingParameters = new OrderSortingParameters
                {
                    SortingAttribute = queryParameters.SortingAttribute,
                    SortingType = queryParameters.SortingType
                },
                StartOrderDate = queryParameters.StartOrderDate
            };
        }
    }

    public class OrderQueryParameters : QueryParameters
    {
        public DateTime? StartOrderDate { get; set; }
        public DateTime? EndOrderDate { get; set; }
        public string? OrderNumber { get; set; }
        public OrderSortingParameters? SortingParameters { get; set; }
    }

    public class OrderSortingParameters
    {
        public SortingType SortingType { get; set; } = SortingType.Descending;
        public OrderSortingAttribute SortingAttribute { get; set; } =
            OrderSortingAttribute.CreationDate;
    }

    public enum OrderSortingAttribute
    {
        CreationDate,
        Number
    }
}
