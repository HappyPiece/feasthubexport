﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO.View
{
    public class PagedView<T>
    {
        public List<T> Items { get; set; } = new();
        public PageInfo Info { get; set; } = new PageInfo();

    }
    public class PageInfo
    {
        public int CurrentPage { get; set; } = 1;
        public int PageCount { get; set; } = 1;
        public int PageSize { get; set; } = default;
    }
}
