﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateUserDTO
    {
        [Display(Name = "Электронная почта")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Введенное значение не является корректным email-адресом")]
        public string Email { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Display(Name = "ФИО")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Пол")]
        public Gender Gender { get; set; }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string? PhoneNumber { get; set; } = null;

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; } = null;
    }

    public class CreateUserWithRolesDTO : CreateUserDTO
    {
        [Display(Name = "Роли")]
        public List<FeastHubRoleType> Roles { get; set; } = new();
    }

    public class EditUserWithRolesDTO : EditUserDTO
    {
        [Display(Name = "Роли")]
        public List<FeastHubRoleType> Roles { get; set; } = new();
    }

    public class CreateCustomerDTO : CreateUserDTO
    {
        public CustomerAssociateDTO? CustomerSelf { get; set; }
    }

    public class UserDTO
    {
        public DateTime? BannedAt { get; set; } = null;
        public bool IsBanned => BannedAt != null;
        public Guid Id { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public string Name { get; set; }
        public Gender Gender { get; set; }
        public string? PhoneNumber { get; set; }
        public DateTime? BirthDate { get; set; }

        public List<FeastHubRoleType> Roles { get; set; } = new ();

        public CustomerAssociateDTO? CustomerSelf { get; set; }
        public ManagerAssociateDTO? ManagerSelf { get; set; }
        public CookAssociateDTO? CourierSelf { get; set; }
        public CourierAssociateDTO? CookSelf { get; set; }
    }
    public class EditUserDTO
    {
        [Display(Name = "ФИО")]
        public string? Name { get; set; }

        [Display(Name = "Пол")]
        public Gender? Gender { get; set; }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string? PhoneNumber { get; set; }

        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        public DateTime? BirthDate { get; set; }
    }

    public enum Gender
    {
        Male = 0,
        Female = 1,
        ApacheAttackHelicopter = 69
    }

    public class EditPasswordDTO
    {
        [Display(Name = "Электронная почта")]
        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Введенное значение не является корректным email-адресом")]
        public string Email { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        [Display(Name = "Новый Пароль")]
        [DataType(DataType.Password)]
        [Required]
        public string NewPassword { get; set; }
    }
}
