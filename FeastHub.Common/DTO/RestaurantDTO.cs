﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateRestaurantDTO
    {
        [Required(ErrorMessage = "Поле обязательно")]
        [MinLength(3, ErrorMessage = "Минимальная длина 3")]
        [MaxLength(50, ErrorMessage = "Максимальная длина 50")]
        public string Name { get; set; }
    }

    public class RestaurantDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<MenuDTO> Menus { get; set; }
        public List<DishDTO>? Dishes { get; set; }
        public List<OrderDTO>? Orders { get; set; }
        public List<Guid>? Cooks { get; set; }
        public List<Guid>? Managers { get; set; }
        public List<string> Categories { get; set; } = new();
        public double Rating { get; set; }
    }

    public class EditRestaurantDTO
    {
        [MinLength(3, ErrorMessage = "Минимальная длина 3")]
        [MaxLength(50, ErrorMessage = "Максимальная длина 50")]
        public string? Name { get; set; }
    }
}
