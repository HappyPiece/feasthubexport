﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public enum FeastHubRoleType
    {
        [Display(Name = "Клиент")]
        Customer,
        [Display(Name = "Менеджер")]
        Manager,
        [Display(Name = "Курьер")]
        Courier,
        [Display(Name = "Повар")]
        Cook,
        [Display(Name = "Администратор")]
        Administrator,
        [Display(Name = "Рут")]
        Root
    }

    public static partial class RoleHierarchyExtension
    {
        public static readonly Dictionary<FeastHubRoleType, HashSet<FeastHubRoleType>> Hierarchy =
            new Dictionary<FeastHubRoleType, HashSet<FeastHubRoleType>>()
            {
                {
                    FeastHubRoleType.Administrator,
                    new HashSet<FeastHubRoleType>()
                    {
                        FeastHubRoleType.Customer,
                        FeastHubRoleType.Courier,
                        FeastHubRoleType.Cook,
                        FeastHubRoleType.Manager
                    }
                },
                {
                    FeastHubRoleType.Root,
                    new HashSet<FeastHubRoleType>()
                    {
                        FeastHubRoleType.Administrator
                    }
                }
            };
        /// <summary>
        ///     Is used to tell whether one user can operate (e.g. ban) the other.
        ///     It is considered to be the case when for every role of the object, the operator has at least one 
        ///     role that is "higher" in the defined hierarchy, i.e. when it grants the right to assign the role being addressed.
        /// </summary>
        /// <returns>Value, indicating whether a holder of specified operator roles can operate the holder of the object roles</returns>
        public static bool CanOperate(this IEnumerable<FeastHubRoleType> operatorRoles, IEnumerable<FeastHubRoleType> objectRoles)
        {
            foreach (var role in objectRoles)
            {
                if (!operatorRoles.Any(x => x.IsComparableWith(role) && x.CanAssign(role))) return false;
            }
            return true;
        }

        /// <returns>Value, indicating whether a holder of specified role can assign the one in view</returns>
        public static bool IsComparableWith(this FeastHubRoleType left, FeastHubRoleType right)
        {
            return (left == right || left.CanAssign(right) || left.CanBeAssignedBy(right));
        }

        /// <returns>Value, indicating whether a holder of specified roles can assign the one in view</returns>
        public static bool CanAssign(this IEnumerable<FeastHubRoleType> grantorRoles, FeastHubRoleType grantedRole)
        {
            return grantedRole.CanBeAssignedBy().Intersect(grantorRoles).Count() >= 1;
        }

        /// <returns>Value, indicating whether a holder of specified roles can assign the ones in view</returns>
        public static bool CanAssign(this IEnumerable<FeastHubRoleType> grantorRoles, IEnumerable<FeastHubRoleType> grantedRoles)
        {
            foreach (var grantedRole in grantedRoles)
            {
                if (!grantorRoles.CanAssign(grantedRole)) return false;
            }
            return true;
        }

        /// <returns>Value, indicating whether a holder of specified role can assign the one being assigned</returns>
        public static bool CanAssign(this FeastHubRoleType grantorRole, FeastHubRoleType grantedRole)
        {
            return (new HashSet<FeastHubRoleType>() { grantorRole }).CanAssign(grantedRole);
        }

        /// <returns>IEnumerable of roles that can be assigned by a holder of specified ones</returns>
        public static IEnumerable<FeastHubRoleType> CanAssign(this IEnumerable<FeastHubRoleType> grantorRoles)
        {
            HashSet<FeastHubRoleType> result = new();
            foreach (var role in grantorRoles)
            {
                result.UnionWith(role.CanAssign());
            }
            return result;
        }

        /// <returns>IEnumerable of roles that can be assigned by a holder of specified one</returns>
        public static IEnumerable<FeastHubRoleType> CanAssign(this FeastHubRoleType grantorRole)
        {
            HashSet<FeastHubRoleType> result = new();
            foreach (var role in Enum.GetValues(typeof(FeastHubRoleType)).Cast<FeastHubRoleType>())
            {
                if (grantorRole.CanAssign(role)) result.Add(role);
            }
            return result;
        }

        /// <returns>Value, indicating whether the specified role can be assigned by a holder of the given grantor role</returns>
        public static bool CanBeAssignedBy(this FeastHubRoleType grantedRole, FeastHubRoleType grantorRole)
        {
            return grantedRole.CanBeAssignedBy().Contains(grantorRole);
        }

        /// <returns>IEnumerable of roles that the specified one can be assigned by</returns>
        public static IEnumerable<FeastHubRoleType> CanBeAssignedBy(this FeastHubRoleType role)
        {
            HashSet<FeastHubRoleType> result = new(), intermediate = new(), current = new() { role };
            while (current.Count > 0)
            {
                foreach (var currentRole in current)
                {
                    intermediate.UnionWith(Hierarchy.Where(x => x.Value.Contains(currentRole)).Select(x => x.Key));
                }
                result.UnionWith(intermediate);
                current = intermediate;
                intermediate = new();
            }
            return result;
        }
    }
}
