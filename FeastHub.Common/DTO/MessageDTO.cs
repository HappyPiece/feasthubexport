﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class NotificationDTO
    {
        public Guid AddresseeId { get; set; }
        public string Message { get; set; }
    }
}
