﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateMenuDTO
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string? Description { get; set; }
        public string? PhotoUrl { get; set; }
        public List<Guid> Dishes { get; set; } = new();
    }

    public class MenuDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string? Description { get; set; }
        public string? PhotoUrl { get; set; }
        public List<DishDTO> Dishes { get; set; } = new();
    }

    public class EditMenuDTO
    {
        public string? Name { get; set; }
        public bool? IsActive { get; set; }
        public string? Description { get; set; }
        public string? PhotoUrl { get; set; }
    }
}
