﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateRatingDTO
    {
        [Range(1, 10)]
        public int Score { get; set; }
    }
    public class RatingDTO
    {
        public Guid Id { get; set; }
        public Guid DishId { get; set; }
        public int Score { get; set; }
        public Guid CustomerId { get; set; }
    }

    public class EditRatingDTO
    {
        [Range(1, 10)]
        public int Score { get; set; }
    }
}
