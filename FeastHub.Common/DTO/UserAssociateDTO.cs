﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public abstract class UserAssociateDTO
    {
        public Guid Id { get; set; }
    }
    public class CustomerAssociateDTO : UserAssociateDTO
    {
        public string? Address { get; set; }
    }

    public class ManagerAssociateDTO : UserAssociateDTO
    {
    }

    public class CookAssociateDTO : UserAssociateDTO
    {
    }

    public class CourierAssociateDTO : UserAssociateDTO
    {
    }
}
