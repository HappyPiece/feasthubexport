﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class LoginDTO
    {
        [Display(Name = "Электронная почта")]
        [Required(ErrorMessage = "Поле обязательно")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Введенное значение не является корректным email-адресом")]
        public string Email { get; set; }

        [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Поле обязательно")]
        public string Password { get; set; }
    }

    public class TokenPairDTO
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }

    }
}
