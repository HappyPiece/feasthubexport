﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CartDTO
    {
        public Guid Id { get; set; }
        public Dictionary<Guid, int> Content { get; set; } = new();
        public double? CartTotal { get; set; }
    }
}
