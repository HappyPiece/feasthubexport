﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.DTO
{
    public class CreateOrderDTO
    {
        public string Address { get; set; }
    }
    public class OrderDTO
    {
        public string Number { get; set; }
        public Guid Id { get; set; }
        public DateTime? DeliveredAt { get; set; }
        public CartDTO Cart { get; set; }
        public string Address { get; set; }
        public double? OrderTotal => Cart.CartTotal;
        public OrderStatus Status { get; set; }
        public Guid RestaurantId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid? CookId { get; set; }
        public Guid? CourierId { get; set; }
    }
    public enum OrderStatus
    {
        Created,
        Cooking,
        Packaged,
        Delivering,
        Done,
        Canceled
    }
}
