﻿using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Exceptions.ExceptionHandler
{
    public class ExceptionInterceptorMiddleware
    {
        private readonly RequestDelegate _next;
        public ExceptionInterceptorMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            var message = ex switch
            {
                _ => "Something went wrong"
            };

            await context.Response.WriteAsync(new ErrorDetails(context.Response.StatusCode, message).ToString());
        }
    }
}
