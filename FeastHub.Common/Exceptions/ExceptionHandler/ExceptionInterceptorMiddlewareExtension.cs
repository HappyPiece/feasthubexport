﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Exceptions.ExceptionHandler
{
    public static class ExceptionInterceptorMiddlewareExtension
    {
        public static void UseExceptionInterceptorMiddleware(this WebApplication app)
        {
            app.UseMiddleware<ExceptionInterceptorMiddleware>();
        }
    }
}
