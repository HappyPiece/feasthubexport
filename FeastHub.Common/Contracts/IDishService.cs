﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IDishService
    {
        Task<OperationResult> CreateDish(Guid restaurantId, CreateDishDTO dto);
        Task<OperationResult<DishDTO>> GetDish(Guid dishId);
        Task<OperationResult> EditDish(Guid dishId, EditDishDTO dto);
        Task<OperationResult> DeleteDish(Guid dishId);
    }
}
