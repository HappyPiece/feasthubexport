﻿using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IRestaurantManagementService
    {
        public Task<OperationResult<RestaurantDTO>> GetRestaurant(Guid id);
        public Task<OperationResult<PagedView<RestaurantShortDTO>>> GetRestaurantsPaged(RestaurantQueryParameters query);

        public Task<OperationResult> CreateRestaurant(CreateRestaurantDTO dto);
        public Task<OperationResult> EditRestaurant(Guid id, EditRestaurantDTO dto);
        public Task<OperationResult<List<UserDTO>>> GetPersonnel(Guid id, FeastHubRoleType role);
        public Task<OperationResult> EditPersonnel(Guid id, Guid userId, ActionType actionType, FeastHubRoleType role);
        public Task<OperationResult> DeleteRestaurant(Guid id);
    }
}
