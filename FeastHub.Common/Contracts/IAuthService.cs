﻿using FeastHub.Common.DTO;
using FeastHub.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IAuthService
    {
        public Task<OperationResult<TokenPairDTO>> Register(CreateUserDTO dto);   
        public Task<OperationResult<TokenPairDTO>> Login(LoginDTO dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns cref="Exceptions.BadTokenException"></returns>
        /// <returns cref="Exceptions.NotFoundException"></returns>
        public Task<OperationResult<TokenPairDTO>> Refresh(TokenPairDTO dto);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns cref="Exceptions.BadTokenException"></returns>
        public Task<OperationResult> Logout(Guid id);
    }
}
