﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IRatingService
    {
        public Task<OperationResult> CreateDishRating(Guid dishId, Guid customerId, CreateRatingDTO dto);
        public Task<OperationResult> EditDishRating(Guid dishId, Guid customerId, EditRatingDTO dto);
        public Task<OperationResult> DeleteDishRating(Guid dishId, Guid customerId);
    }
}
