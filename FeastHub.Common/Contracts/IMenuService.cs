﻿using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IMenuService
    {
        Task<OperationResult> CreateMenu(CreateMenuDTO dto, Guid restaurantId);
        Task<OperationResult<MenuDTO>> GetMenu(Guid id);
        Task<OperationResult> EidtMenu(Guid id, EditMenuDTO dto);
        Task<OperationResult> DeleteSenu(Guid id);
    }
}
