﻿using FeastHub.Common.DTO;

namespace FeastHub.Common.Contracts
{
    public interface INotificationService
    {
        public Task<OperationResult> SendNotification(NotificationDTO dto);
    }
}
