﻿using FeastHub.Common.DTO;
using FeastHub.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Common.Contracts
{
    public interface IUserService
    {
        public Task<OperationResult<UserDTO>> GetProfile(Guid id);
        public Task<OperationResult> EditProfile(Guid id, EditUserDTO dto);
        public Task<OperationResult> ChangePassword(EditPasswordDTO dto);
    }
}
