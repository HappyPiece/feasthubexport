﻿using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IOrderService
    {
        public Task<OperationResult<PagedView<OrderDTO>>> GetOrders(Guid ownerId, FeastHubRoleType callerRole, OrderQueryParameters query);

        Task<OperationResult> CreateOrder(Guid customerId, CreateOrderDTO dto);
        Task<OperationResult<CartDTO>> GetCart(Guid customerId);
        public Task<OperationResult> SetDishNumberInCart(Guid customerId, Guid dishId, int number); 
        public  Task<OperationResult> CopyOrderContent(Guid customerId, Guid orderId);

        public Task<OperationResult> TakeToKitchen(Guid orderId, Guid cookId);
        public Task<OperationResult> PutForCourier(Guid orderId, Guid cookId);
        public Task<OperationResult> TakeForDelivery(Guid orderId, Guid courierId);   
        public Task<OperationResult> MarkAsDone(Guid orderId, Guid courierId);
        public Task<OperationResult> Cancel(Guid orderId, Guid userId);

        public Task<OperationResult<List<OrderDTO>>> GetCourierAvailableOrders(Guid courierId);
        public Task<OperationResult<List<OrderDTO>>> GetCookAvailableOrders(Guid cookId);
    }
}
