﻿using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Contracts
{
    public interface IRestaurantService
    {
        public Task<OperationResult<RestaurantDTO>> GetRestaurant(Guid id);
        public Task<OperationResult> CanManage(Guid restaurantId, Guid userId);
        public Task<OperationResult> CanManageDish(Guid dishId, Guid userId);
        public Task<OperationResult> CanManageMenu(Guid menuId, Guid userId);
        public Task<List<RestaurantShortDTO>> GetRestaurants();
        public Task<OperationResult<PagedView<RestaurantShortDTO>>> GetRestaurantsPaged(RestaurantQueryParameters query);

        public Task<OperationResult> CreateCategories(Guid restaurantId, List<string> categories);
        public Task<OperationResult> EditCategories(Guid restaurantId, Dictionary<string, string> categories);
        public Task<OperationResult> DeleteCategories(Guid restaurantId, List<string> categories);

        public Task<OperationResult> CreateMenu(Guid restaurantId, CreateMenuDTO dto);
        public Task<OperationResult> EditMenu(Guid menuId, EditMenuDTO dto);
        public Task<OperationResult> DeleteMenu(Guid menuId);
        public Task<OperationResult> AddDishesToMenu(Guid menuId, List<Guid> dishIds);
        public Task<OperationResult> RemoveDishesFromMenu(Guid menuId, List<Guid> dishIds);

        public Task<OperationResult> CreateRestaurant(CreateRestaurantDTO dto);
        public Task<OperationResult<PagedView<DishDTO>>> GetRestaurantDishes(Guid restaurantId, DishQueryParameters query);
    }
}
