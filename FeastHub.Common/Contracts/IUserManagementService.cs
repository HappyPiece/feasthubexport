﻿using FeastHub.Common.DTO;
using FeastHub.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.DTO.View;
using System.Security.Claims;

namespace FeastHub.Common.Contracts
{
    public interface IUserManagementService
    {
        public Task<OperationResult<UserDTO>> GetUser(Guid userId);
        public Task<OperationResult<PagedView<UserDTO>>> GetUsers(UserQueryParameters query);
        public Task<OperationResult> CreateUser(CreateUserWithRolesDTO dto, ClaimsPrincipal initiator);
        public Task<OperationResult> ManageRole(Guid userId, FeastHubRoleType role, ActionType actionType, ClaimsPrincipal initiator);
        public Task<OperationResult> SyncRoles(Guid userId, List<FeastHubRoleType> roles, ClaimsPrincipal initiator);
        public Task<OperationResult> BanUser(Guid userId, ActionType actionType, ClaimsPrincipal initiator);
    }
}
