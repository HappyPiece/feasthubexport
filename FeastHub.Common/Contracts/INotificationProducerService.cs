﻿using FeastHub.Common.DTO;

namespace FeastHub.Common.Contracts
{
    public interface INotificationProducerService
    {
        public void SendNotification(NotificationDTO dto);
    }
}
