﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;

namespace FeastHub.Common
{
    public static class OpenApiSecurityScheme
    {
        public static Microsoft.OpenApi.Models.OpenApiSecurityScheme JwtSecurityScheme = new()
        {
            BearerFormat = "Jwt",
            Name = "Jwt Authentication",
            In = ParameterLocation.Header,
            Type = SecuritySchemeType.Http,
            Scheme = JwtBearerDefaults.AuthenticationScheme,
            Description = "Enter your JWT Bearer token in the textbox below",

            Reference = new OpenApiReference
            {
                Id = JwtBearerDefaults.AuthenticationScheme,
                Type = ReferenceType.SecurityScheme
            }
        };
    }
}
