﻿using FeastHub.Common.Exceptions;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Utilities
{
    public static class OperationResultUtilities
    {
        /// <summary>
        /// Used to log an exception describing an error and return corresponding result
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static ObjectResult DefaultEvaluator(Exception exception)
        {
            return exception switch
            {
                NotFoundException => new NotFoundObjectResult(exception.Message),
                AlreadyExistsException => new ConflictObjectResult(exception.Message),
                InvalidCredentialsException => new BadRequestObjectResult(exception.Message),
                BadTokenException => new BadRequestObjectResult(exception.Message),
                AccessDeniedException => new ObjectResult(exception.Message) { StatusCode = 403 },
                InvalidOperationException => new BadRequestObjectResult(exception.Message),
                IdentityException e => new BadRequestObjectResult(e.Errors),
                IAmATeapotException => new ObjectResult(exception.Message) { StatusCode = 418 },
                _ => new Func<ObjectResult>(() =>
                {
                    Console.WriteLine(exception);
                    return new ObjectResult("Something went wrong") { StatusCode = 500 };
                })()
            };
        }
    }
}
