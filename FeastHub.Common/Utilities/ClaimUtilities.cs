﻿using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Utilities
{
    public static class ClaimUtilities
    {
        public static List<FeastHubRoleType> ExtractRoles(this ClaimsPrincipal principal)
        {
            var result = new List<FeastHubRoleType>();
            foreach (var claim in principal.Claims)
            {
                if (claim.Type == ClaimConfiguration.RoleClaimType)
                {
                    if (!Enum.TryParse(claim.Value, false, out FeastHubRoleType role))
                        throw new ArgumentException("Role claim cannot be parsed into application role");
                    result.Add(role);
                }
            }
            return result;
        }
    }
}
