﻿using FeastHub.Common.Exceptions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace FeastHub.Common.Utilities
{
    public static class ModelUtilities
    {
        public static void UpdateFromResult(this ModelStateDictionary modelState, OperationResult operationResult)
        {
            if (operationResult.IsFailed)
            {
                if (operationResult.Exception is IdentityException)
                {
                    foreach (var error in (operationResult.Exception as IdentityException)!.Errors!)
                    {
                        modelState.AddModelError("OperationErrors", error.Description);
                    }
                }
                else modelState.AddModelError("OperationErrors", operationResult.Exception!.Message);
            }
        }
    }
}
