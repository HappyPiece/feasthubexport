﻿using FeastHub.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Utilities
{
    public static class GuidUtilities
    {
        public static Guid ExtractGuidFromClaimsPrincipal(ClaimsPrincipal user)
        {
            if (user.Identity is null)
            {
                throw new ArgumentNullException("User.Identity", "Expected not null User.Identity");
            }
            if (!Guid.TryParse(user.Identity.Name, out var id))
            {
                throw new BadTokenException("Expected User.Identity.Name to be valid string representation of Guid");
            }
            return id;
        }
    }
}
