﻿using FeastHub.Common.Configurations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Common.Utilities
{
    public static class PagedViewUtilities
    {
        public static int CountPages(int itemCount, int pageSize)
        {
            return Math.Max((itemCount + pageSize * ((itemCount % pageSize > 0) ? 1 : 0)) / pageSize, 1);
        }

        public static bool PageExists(int pageNumber, int itemCount, int pageSize)
        {
            if ((pageNumber < 1) || (itemCount - (pageNumber - 1) * pageSize <= 0) && pageNumber != 1)
            {
                return false;
            }
            return true;
        }
    }
}
