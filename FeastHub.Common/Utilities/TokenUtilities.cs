﻿using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace FeastHub.Common.Utilities
{
    public static class TokenUtilities
    {
        public static TokenValidationParameters GenerateDefaultTokenValidationParameters(AppTokenOptions tokenOptions)
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = tokenOptions.Issuer,
                ValidateAudience = true,
                ValidAudience = tokenOptions.Audience,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSymmetricSecurityKey(tokenOptions),
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };
        }
        public static string GenerateAccessToken(IEnumerable<Claim> claims, AppTokenOptions options)
        {
            var signingCredentials = new SigningCredentials(GetSymmetricSecurityKey(options),
                SecurityAlgorithms.HmacSha256);

            var tokenOptions = new JwtSecurityToken(
                options.Issuer,
                options.Audience,
                signingCredentials: signingCredentials,
                claims: claims,
                expires: DateTime.UtcNow.AddAccessTokenLifetime(options));

            return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
        }

        public static string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        /// <summary>
        /// Extracts a principal from a token
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        /// <exception cref="SecurityTokenInvalidTypeException"></exception>
        public static ClaimsPrincipal GetPrincipalFromToken(string token, AppTokenOptions options)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = options.Issuer,
                ValidateAudience = true,
                ValidAudience = options.Audience,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSymmetricSecurityKey(options),
                ValidateLifetime = false
            };

            var principal =
                new JwtSecurityTokenHandler().ValidateToken(token, tokenValidationParameters, out var securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken ||
                !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                    StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenInvalidTypeException();

            return principal;
        }

        /// <summary>
        /// Creates a token pair for a user given a list of it's claims
        /// </summary>
        /// <param name="claims"></param>
        /// <returns></returns>
        public static TokenPairDTO GenerateTokenPair(List<Claim> claims, AppTokenOptions options)
        {
            var accessToken = GenerateAccessToken(claims, options);
            var refreshToken = GenerateRefreshToken();
            return new TokenPairDTO
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
        }

        public static SymmetricSecurityKey GetSymmetricSecurityKey(AppTokenOptions options)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(options.SecretKey));
        }
        public static DateTime AddAccessTokenLifetime(this DateTime dateTime, AppTokenOptions options)
        {
            return dateTime.AddMinutes(options.AccessTokenLifetime);
        }
        public static DateTime AddRefreshTokenLifetime(this DateTime dateTime, AppTokenOptions options)
        {
            return dateTime.AddDays(options.RefreshTokenLifetime);
        }
    }
}
