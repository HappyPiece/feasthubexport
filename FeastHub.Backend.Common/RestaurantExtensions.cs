﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common.DTO.View.QueryParameters;
using Microsoft.EntityFrameworkCore;

namespace FeastHub.Backend.BL.Common
{
    public static class IQueryableRestaurantExtension
    {
        public static IOrderedQueryable<Restaurant> SortRestaurants(this IQueryable<Restaurant> queryable, RestaurantSortingParameters parameters)
        {
            return parameters.SortingAttribute switch
            {
                RestaurantSortingAttribute.Rating => new Func<IQueryable<Restaurant>, IOrderedQueryable<Restaurant>>((IQueryable<Restaurant> dishes) =>
                {
                    return parameters?.SortingType is SortingType.Descending ? dishes.OrderByDescending(x => x.Rating) : dishes.OrderBy(x => x.Rating);
                })(queryable),

                _ => new Func<IQueryable<Restaurant>, IOrderedQueryable<Restaurant>>((IQueryable<Restaurant> dishes) =>
                {
                    return parameters?.SortingType is SortingType.Descending ? dishes.OrderByDescending(x => x.Name) : dishes.OrderBy(x => x.Name);
                })(queryable),
            };
        }

        public static IQueryable<Restaurant> FilterName(this IQueryable<Restaurant> queryable, string? name)
        {
            if (name == null)
            {
                return queryable;
            }

            return queryable.Where(x => EF.Functions.Like(x.Name.ToLower(), $"%{name.ToLower()}%"));
        }
    }
}