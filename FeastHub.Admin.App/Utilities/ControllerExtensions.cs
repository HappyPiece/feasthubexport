﻿using FeastHub.Admin.App.ViewModels;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.Admin.App.Utilities
{
    public static class ControllerExtensions
    {
        public static RedirectToActionResult ToError(this ControllerBase controller, Exception exception)
        {
            return controller.RedirectToAction("Fail", "Error", OperationResultViewModel.GenerateFromResult(OperationResultUtilities.DefaultEvaluator(exception)));
        }
    }
}
