let query, newQuery;
$(document).ready(initUserModal);

function initUserModal() {
    $.ajaxSetup({
        contentType: 'application/json'
    });

    reInitUserModal();
    loadModal(query);
}

function reInitUserModal() {
    let roles = [];
    ($("[name='Query.Roles']").each((number, checkbox) => {
        if (checkbox.checked) {
            roles.push($(checkbox).val());
        }
    }));
    query = new Query($("[page-number]").val(),
        $("[user-name]").val(),
        $("[user-email]").val(),
        $("[sorting-attribute]").val(),
        $("[sorting-type]").val(),
        roles);
    newQuery = new Query(1,
        $("[user-name]").val(),
        $("[user-email]").val(),
        $("[sorting-attribute]").val(),
        $("[sorting-type]").val(),
        roles);
    setup();
}

function loadModal(query) {
    $(".modal").load(baseUrl() + "/restaurant/users" + query.queryString(), () => {
        reInitUserModal();
        setUserModalListeners();
    });
}

function setup() {
    $("[user-email]").trigger("input");
    $("[user-name]").trigger("input");
}

function editPersonnel(userId, actionType, role) {
    let restaurantId = $("[restaurant-id]").attr("restaurant-id");
    console.log(`${baseUrl()}/restaurant/editpersonnel/${restaurantId}` +
        `?userId=${userId}&actionType=${actionType}&role=${role}`);
    $.post(`${baseUrl()}/restaurant/editpersonnel/${$("[restaurant-id]").attr("restaurant-id")}` +
        `?userId=${userId}&actionType=${actionType}&role=${role}`)
        .done(() => {
            $("#personnel-container").load(`${baseUrl()}/restaurant/restaurant/${restaurantId} #personnel-content`, () => {
                setRestaurantListeners();
            });
        })
        .fail((jqXHR, textStatus, errorThrown) => {
            console.log(errorThrown);
        });
}

function setUserModalListeners() {

    $("[add-user]").on("click", (e) => {
        editPersonnel(e.target.getAttribute("add-user"), "Add", $("[where-to]").val());
    });

    $("[name='Query.Roles']").on("change", (e) => {
        let target = $(e.target);
        if (e.target.checked) {
            newQuery.roles.push(target.val());
        }
        else {
            newQuery.roles = newQuery.roles.filter(x => x != target.val());
        }
    });

    $("#search-button").on("click", () => { search() });
    $("#clear-name").on("click", () => {
        $("[user-name]").val('');
        $("[user-name]").trigger("input");
    });
    $("#clear-email").on("click", () => {
        $("[user-email]").val('');
        $("[user-email]").trigger("input");
    });
    $("[user-name]").on("input", (e) => {
        if ($(e.target).val() == '') $("#clear-name").addClass("disabled"); else $("#clear-name").removeClass("disabled");
        newQuery.name = $(e.target).val()
    });
    $("[user-email]").on("input", (e) => {
        if ($(e.target).val() == '') $("#clear-email").addClass("disabled"); else $("#clear-email").removeClass("disabled");
        newQuery.email = $(e.target).val()
    });
    $("[sorting-attribute]").on("input", (e) => { newQuery.sortingAttribute = $(e.target).val() });
    $("[sorting-type]").on("input", (e) => { newQuery.sortingType = $(e.target).val() });
    $("[to-page]").on("click", (e) => {
        query.page = $(e.target).attr("to-page");
        loadModal(query);
    });
}

function search() {
    console.log(newQuery.queryString());
    loadModal(newQuery);
}

function baseUrl() {
    return window.location.protocol + "//" + window.location.host;
}

class Query {
    constructor(page = 1, name = "", email = "", sortingAttribute = null, sortingType = null, roles = []) {
        this.sortingAttribute = sortingAttribute;
        this.sortingType = sortingType;
        this.page = page;
        this.name = name;
        this.email = email;
        this.roles = roles;
    }

    queryString() {
        let result = "?"
        for (let attr in this) {
            //console.log(`${attr}=${this[attr]}`);
            if (Array.isArray(this[attr])) {
                for (let value of this[attr]) {
                    result += `${attr}=${value}&`
                }
            }
            else if (this[attr]) result += `${attr}=${this[attr]}&`;
        }
        if (result.slice(-1) == '&' || result.slice(-1) == '?') result = result.slice(0, -1);
        return result;
    }
}