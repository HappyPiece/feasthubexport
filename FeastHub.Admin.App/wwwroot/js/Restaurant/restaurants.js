let query, newQuery;
$(document).ready(init);

function init() {
    $.ajaxSetup({
        contentType: 'application/json'
    });
    query = new Query($("[page-number]").val(), $("[restaurant-name]").val(), $("[sorting-attribute]").val(), $("[sorting-type]").val());
    newQuery = new Query(1, $("[restaurant-name]").val(), $("[sorting-attribute]").val(), $("[sorting-type]").val());
    setRestautantsListeners();
    setup();
}

function setup() {
    $("[restaurant-name]").trigger("input");
}

function setRestautantsListeners() {
    $("#search-button").on("click", () => { search() });
    $("#clear-name").on("click", () => {
        $("[restaurant-name]").val('');
        $("[restaurant-name]").trigger("input");
    });
    $("[restaurant-name]").on("input", (e) => {
        if ($(e.target).val() == '') $("#clear-name").addClass("disabled"); else $("#clear-name").removeClass("disabled");
        newQuery.name = $(e.target).val()
    });
    $("[sorting-attribute]").on("input", (e) => { newQuery.sortingAttribute = $(e.target).val() });
    $("[sorting-type]").on("input", (e) => { newQuery.sortingType = $(e.target).val() });
    $("[to-page]").on("click", (e) => {
        query.page = $(e.target).attr("to-page");
        window.location.assign(baseUrl() + "/restaurant/restaurants" + query.queryString());
    });
}

function search() {
    window.location.assign(baseUrl() + "/restaurant/restaurants" + newQuery.queryString());
}

function baseUrl() {
    return window.location.protocol + "//" + window.location.host;
}

class Query {
    constructor(page = 1, name = null, sortingAttribute = null, sortingType = null) {
        this.sortingAttribute = sortingAttribute;
        this.sortingType = sortingType;
        this.page = page;
        this.name = name;
    }

    queryString() {
        let result = "?"
        for (let attr in this) {
            if (this[attr]) result += `${attr}=${this[attr]}&`;
            console.log(`${attr}=${this[attr]}`);
        }
        if (result.slice(-1) == '&' || result.slice(-1) == '?') result = result.slice(0, -1);
        return result;
    }
}