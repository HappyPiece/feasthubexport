$(document).ready(init);

function init() {  
    setRestaurantListeners();
}


function setRestaurantListeners() {
    $("[open]").on("click", (e) => {
        $("[where-to]").val(e.target.getAttribute("open"));
        $("#add-personnel").modal('show');
    });
    $("[dismiss]").on("click", (e) => {
        editPersonnel(e.target.getAttribute("dismiss"), "Remove", $(e.target).parents("[personnel-role]").attr("personnel-role"));
    });
}

function baseUrl() {
    return window.location.protocol + "//" + window.location.host;
}