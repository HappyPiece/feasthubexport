using System.Text.Json.Serialization;
using System.Text.Json;
using FeastHub.Admin.BL;
using Microsoft.AspNetCore.Authentication.Cookies;
using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;

var builder = WebApplication.CreateBuilder(args);

builder.Services.BindOptions<PagedViewOptions>(builder.Configuration.GetSection("PagedView"));

builder.Services.AddControllersWithViews().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddAdminBL(userDbConnectionString: builder.Configuration.GetRequiredSection("UserDb").Get<DbOptions>()!.Connection,
    backendDbConnectionString: builder.Configuration.GetRequiredSection("BackendDb").Get<DbOptions>()!.Connection);

builder.Services.AddRazorPages().AddRazorRuntimeCompilation();
builder.Services.AddHealthChecks();
builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.LoginPath = new PathString("/Auth/Login");
        options.LogoutPath = new PathString("/Auth/Logout");
    });

var app = builder.Build();
await app.UseAdminBL(builder.Configuration.GetSection("Root").Get<CreateUserDTO>());

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "restaurants",
    pattern: "restaurant/restaurants");

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");


app.MapHealthChecks("/health");

app.Run();
