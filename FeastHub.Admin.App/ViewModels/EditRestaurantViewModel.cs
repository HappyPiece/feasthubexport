using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Admin.App.ViewModels
{
    public class EditRestaurantViewModel
    {
        public Guid Id { get; set; }
        public EditRestaurantDTO DTO { get; set; }
    }
}