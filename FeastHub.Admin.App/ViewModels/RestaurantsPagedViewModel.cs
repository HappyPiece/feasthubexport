using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Admin.App.ViewModels
{
    public class RestaurantsPagedViewModel
    {
        public PagedView<RestaurantShortDTO> Content { get; set; }
        public FlatRestaurantQueryParameters Query { get; set; } = new();
    }
}
