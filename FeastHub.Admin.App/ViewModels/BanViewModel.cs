using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Admin.App.ViewModels
{
    public class BanViewModel
    {
        public UserDTO User { get; set; }

        public ActionType ActionType { get; set; }
    }
}