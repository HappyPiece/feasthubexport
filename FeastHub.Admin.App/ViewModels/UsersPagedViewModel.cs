using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Admin.App.ViewModels
{
    public class UsersPagedViewModel
    {
        public PagedView<UserDTO> Content { get; set; } = new PagedView<UserDTO>();
        public FlatUserQueryParameters Query { get; set; } = new();
    }
}
