using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.Admin.App.ViewModels
{
    public class RestaurantPersonnelViewModel
    {
        public RestaurantDTO Restaurant { get; set; }
        public List<UserDTO> Cooks { get; set; } = new List<UserDTO>();
        public List<UserDTO> Managers { get; set; } = new List<UserDTO>();
    }
}