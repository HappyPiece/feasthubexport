﻿using FeastHub.Admin.App.ViewModels;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using Microsoft.AspNetCore.Authorization;
using FeastHub.Common;
using FeastHub.Admin.App.Utilities;

namespace FeastHub.Admin.App.Controllers
{
    [Authorize]
    public class RestaurantController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRestaurantManagementService _managementService;
        private readonly IUserManagementService _userManagement;

        public RestaurantController(
            ILogger<HomeController> logger,
            IRestaurantManagementService managementService,
            IUserManagementService userManagement
        )
        {
            _logger = logger;
            _managementService = managementService;
            _userManagement = userManagement;
        }

        public async Task<IActionResult> Users([FromQuery] FlatUserQueryParameters query)
        {
            var result = await _userManagement.GetUsers(query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return PartialView(
                        new UsersPagedViewModel { Content = result.Value!, Query = query }
                    );
                },
                this.ToError
            );
        }

        [HttpPost]
        public async Task<IActionResult> EditPersonnel(
            [FromRoute] Guid id,
            [FromQuery] Guid userId,
            [FromQuery] ActionType actionType,
            [FromQuery] FeastHubRoleType role
        )
        {
            var result = await _managementService.EditPersonnel(id, userId, actionType, role);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new CreateRestaurantDTO());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] CreateRestaurantDTO dto)
        {
            if (!ModelState.IsValid)
                return View();
            var result = await _managementService.CreateRestaurant(dto);
            if (result.IsFailed)
            {
                ModelState.AddModelError("CreateErrors", result.Exception!.Message);
                return View(dto);
            }
            else
            {
                return RedirectToAction("Restaurants");
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit([FromRoute] Guid id)
        {
            var result = await _managementService.GetRestaurant(id);
            if (result.IsFailed)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(
                new EditRestaurantViewModel
                {
                    Id = id,
                    DTO = new EditRestaurantDTO() { Name = result.Value!.Name }
                }
            );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([FromRoute] Guid id, [FromForm] EditRestaurantDTO dto)
        {
            if (!ModelState.IsValid)
                return View();
            var result = await _managementService.EditRestaurant(id, dto);
            if (result.IsFailed)
            {
                ModelState.AddModelError("EditErrors", result.Exception!.Message);
                return View(new EditRestaurantViewModel { Id = id, DTO = dto });
            }
            else
            {
                return RedirectToAction("Restaurants");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var result = await _managementService.GetRestaurant(id);
            if (result.IsFailed)
            {
                return RedirectToAction("NotFound", "Error");
            }
            return View(id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete([FromRoute] Guid id, bool? overload)
        {
            var result = await _managementService.DeleteRestaurant(id);
            if (result.IsFailed)
            {
                ModelState.AddModelError("DeleteErrors", result.Exception!.Message);
                return View(id);
            }
            else
            {
                return RedirectToAction("Restaurants");
            }
        }

        public async Task<IActionResult> Restaurant([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
                return View();
            var result = await GenerateRestaurantWithPersonnel(id);
            ViewBag.Users = new UsersPagedViewModel();
            ViewBag.Model = result.Value!;
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return View(result.Value!);
                },
                this.ToError
            );
        }

        public async Task<IActionResult> Restaurants(
            [FromQuery] FlatRestaurantQueryParameters query
        )
        {
            var result = await _managementService.GetRestaurantsPaged(query);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return View(
                        new RestaurantsPagedViewModel { Content = result.Value!, Query = query }
                    );
                },
                this.ToError
            );
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(
                new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
                }
            );
        }

        private async Task<
            OperationResult<RestaurantPersonnelViewModel>
        > GenerateRestaurantWithPersonnel(Guid id)
        {
            var model = new RestaurantPersonnelViewModel();
            var restaurantResult = await _managementService.GetRestaurant(id);
            if (restaurantResult.IsFailed)
                return new OperationResult<RestaurantPersonnelViewModel>(
                    restaurantResult.Exception!
                );
            model.Restaurant = restaurantResult.Value!;
            var cookResult = await _managementService.GetPersonnel(id, FeastHubRoleType.Cook);
            if (cookResult.IsFailed)
                return new OperationResult<RestaurantPersonnelViewModel>(cookResult.Exception!);
            model.Cooks = cookResult.Value!;
            var managerResult = await _managementService.GetPersonnel(id, FeastHubRoleType.Manager);
            if (managerResult.IsFailed)
                return new OperationResult<RestaurantPersonnelViewModel>(managerResult.Exception!);
            model.Managers = managerResult.Value!;
            return new OperationResult<RestaurantPersonnelViewModel>(model);
        }
    }
}
