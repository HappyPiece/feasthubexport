﻿using FeastHub.Admin.App.ViewModels;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.DTO;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using FeastHub.Common.Contracts;
using Microsoft.AspNetCore.Authorization;
using FeastHub.Common.Exceptions;
using FeastHub.Admin.App.Utilities;

namespace FeastHub.Admin.App.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserManagementService _userManagement;

        public UserController(ILogger<HomeController> logger, IUserManagementService userManagement)
        {
            _logger = logger;
            _userManagement = userManagement;
        }

        [HttpGet]
        public async Task<ActionResult> Edit([FromRoute] Guid id)
        {
            var userResult = await _userManagement.GetUser(id);
            if (userResult.IsFailed)
            {
                return RedirectToAction("NotFound", "Error");
            }
            ViewBag.Id = id;
            ViewBag.Email = userResult.Value!.Email;
            var dto = new EditUserWithRolesDTO
            {
                Roles = userResult.Value.Roles,
                BirthDate = userResult.Value.BirthDate,
                Gender = userResult.Value.Gender,
                Name = userResult.Value.Name,
                PhoneNumber = userResult.Value.PhoneNumber
            };
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(
            [FromRoute] Guid id,
            [FromForm] EditUserWithRolesDTO dto
        )
        {
            var userResult = await _userManagement.GetUser(id);
            if (userResult.IsFailed)
            {
                return RedirectToAction("NotFound", "Error");
            }
            ViewBag.Id = id;
            ViewBag.Email = userResult.Value!.Email;
            if (!ModelState.IsValid)
                return View(dto);
            var result = await _userManagement.SyncRoles(id, dto.Roles, User);
            if (result.IsFailed)
            {
                ModelState.UpdateFromResult(result);
                return View(dto);
            }
            else
            {
                return RedirectToAction("Users");
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new CreateUserWithRolesDTO());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] CreateUserWithRolesDTO dto)
        {
            if (!ModelState.IsValid)
                return View(dto);
            var result = await _userManagement.CreateUser(dto, User);
            if (result.IsFailed)
            {
                ModelState.UpdateFromResult(result);
                return View(dto);
            }
            else
            {
                return RedirectToAction("Users");
            }
        }

        [HttpGet]
        public async Task<ActionResult> Ban([FromRoute] Guid id, [FromQuery] ActionType actionType)
        {
            var userResult = await _userManagement.GetUser(id);
            if (userResult.IsFailed)
            {
                return this.ToError(userResult.Exception!);
            }
            ViewBag.ActionType = actionType;
            return View(userResult.Value!);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Ban(
            [FromRoute] Guid id,
            [FromQuery] ActionType actionType,
            bool? overload = null
        )
        {
            var userResult = await _userManagement.GetUser(id);
            if (userResult.IsFailed)
            {
                return this.ToError(userResult.Exception!);
            }
            ViewBag.ActionType = actionType;
            var user = userResult.Value!;
            var result = await _userManagement.BanUser(user.Id, actionType, User);
            if (result.IsFailed)
            {
                ModelState.UpdateFromResult(result);
                return View(user);
            }
            else
            {
                return RedirectToAction("Users");
            }
        }

        public async Task<IActionResult> Users([FromQuery] FlatUserQueryParameters query)
        {
            var result = await _userManagement.GetUsers(query);

            return result.Evaluate<IActionResult>(
                () =>
                {
                    return View(new UsersPagedViewModel { Content = result.Value!, Query = query });
                },
                this.ToError
            );
        }

        [HttpPost, Route("{userId}/role/{actionType}")]
        public async Task<IActionResult> AssignRole(
            [FromRoute] Guid userId,
            [FromQuery] FeastHubRoleType role,
            [FromRoute] ActionType actionType
        )
        {
            var result = await _userManagement.ManageRole(userId, role, actionType, User);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
