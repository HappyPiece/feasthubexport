﻿using FeastHub.Admin.App.ViewModels;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Claims;

namespace FeastHub.Admin.App.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserManagementService _userService;
        private readonly ICookieAuthService _authService;

        public AccountController(
            ILogger<HomeController> logger,
            IUserManagementService userService,
            ICookieAuthService authService
        )
        {
            _logger = logger;
            _userService = userService;
            _authService = authService;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginDTO());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([FromForm] LoginDTO dto)
        {
            if (!ModelState.IsValid)
                return View(dto);
            var result = await _authService.Login(dto);
            if (result.IsFailed)
            {
                ModelState.AddModelError("LoginErrors", result.Exception!.Message);
                return View(dto);
            }
            else
            {
                await HttpContext.SignInAsync(
                    CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(result.Value!)
                );
                return RedirectToAction("index", "home");
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _authService.Logout();
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("login", "account");
        }
    }
}
