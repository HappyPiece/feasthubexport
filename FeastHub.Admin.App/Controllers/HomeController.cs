﻿using FeastHub.Admin.App.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace FeastHub.Admin.App.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}