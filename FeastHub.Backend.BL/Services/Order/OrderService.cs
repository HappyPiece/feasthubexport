﻿using FeastHub.Backend.BL.Services.Internal;
using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace FeastHubBL.Services
{
    public partial class OrderService : IOrderService
    {
        private readonly FeastHubBackendDbContext _dbContext;
        private readonly IInternalService _internalService;
        private readonly IQueryable<Order> _orders;
        private readonly PagedViewOptions _pagedViewOptions;
        private readonly INotificationProducerService _notificationProducerService;

        public OrderService(
            FeastHubBackendDbContext dbContext,
            IInternalService internalService,
            IOptions<PagedViewOptions> pagedViewOptions,
            INotificationProducerService notificationProducerService
        )
        {
            _dbContext = dbContext;
            _internalService = internalService;
            _pagedViewOptions = pagedViewOptions.Value;
            _orders = _dbContext.Orders
                .Include(x => x.Restaurant)
                .Include(x => x.Cart)
                .ThenInclude(x => x.Piles)
                .ThenInclude(x => x.Dish);
            _notificationProducerService = notificationProducerService;
        }

        public async Task<OperationResult<PagedView<OrderDTO>>> GetOrders(
            Guid ownerId,
            FeastHubRoleType callerRole,
            OrderQueryParameters query
        )
        {
            var ordersQuery = _orders
                .FilterdDate(query.StartOrderDate, query.EndOrderDate)
                .FilterNumber(query.OrderNumber)
                .Where(GetPropertyEvaluator(ownerId, callerRole))
                .Include(x => x.Cart)
                .ThenInclude(x => x.Piles)
                .ThenInclude(x => x.Dish)
                .Include(x => x.Restaurant)
                .ThenInclude(x => x.Orders);

            var count = await ordersQuery.CountAsync();
            if (!PagedViewUtilities.PageExists(query.Page, count, _pagedViewOptions.OrderPageSize))
            {
                return new OperationResult<PagedView<OrderDTO>>(
                    new NotFoundException("Requested page does not exist")
                );
            }

            var orders = await ordersQuery
                .SortOrders(query.SortingParameters)
                .Skip((query.Page - 1) * _pagedViewOptions.OrderPageSize)
                .Take(_pagedViewOptions.RestaurantPageSize)
                .ToListAsync();

            var result = new PagedView<OrderDTO>
            {
                Items = orders.Select(x => (OrderDTO)x).ToList(),
                Info = new PageInfo
                {
                    CurrentPage = query.Page,
                    PageCount = PagedViewUtilities.CountPages(
                        count,
                        _pagedViewOptions.OrderPageSize
                    ),
                    PageSize = orders.Count
                }
            };

            return new OperationResult<PagedView<OrderDTO>>(result);
        }

        private static Expression<Func<Order, bool>> GetPropertyEvaluator(
            Guid ownerId,
            FeastHubRoleType role
        )
        {
            return role switch
            {
                FeastHubRoleType.Customer => (order) => order.CustomerId == ownerId,
                FeastHubRoleType.Courier => (order) => order.CourierId == ownerId,
                FeastHubRoleType.Cook => (order) => order.CookId == ownerId,
                FeastHubRoleType.Manager => (order) => order.Restaurant.Id == ownerId,
                _ => throw new InvalidOperationException()
            };
        }

        public async Task<OperationResult<List<OrderDTO>>> GetCourierAvailableOrders(Guid courierId)
        {
            var orders = await _orders.Where(x => x.Status == OrderStatus.Packaged).ToListAsync();

            return new OperationResult<List<OrderDTO>>(orders.Select(x => (OrderDTO)x).ToList());
        }

        public async Task<OperationResult<List<OrderDTO>>> GetCookAvailableOrders(Guid cookId)
        {
            var restaurants = await _dbContext.Restaurants
                .Where(x => x.Cooks.Any(c => c == cookId))
                .ToListAsync();
            if (restaurants.Count == 0)
            {
                return new OperationResult<List<OrderDTO>>(
                    new InvalidOperationException(
                        $"Cook '{cookId}' does not have available orders since they do not work anywhere lol"
                    )
                );
            }

            var currentRestaurantId = await _dbContext.Orders
                .Where(x => x.CookId == cookId && x.Status == OrderStatus.Cooking)
                .Select(x => x.Restaurant.Id)
                .SingleOrDefaultAsync();

            var ordersQuery = _orders;
            ordersQuery =
                currentRestaurantId == default
                    ? ordersQuery.Where(x => restaurants.Contains(x.Restaurant))
                    : ordersQuery.Where(x => x.Restaurant.Id == currentRestaurantId);
            var orders = await ordersQuery
                .Where(x => x.Status == OrderStatus.Created)
                .ToListAsync();

            return new OperationResult<List<OrderDTO>>(orders.Select(x => (OrderDTO)x).ToList());
        }
    }

    public static class IQueryableOrderExtension
    {
        public static IOrderedQueryable<Order> SortOrders(
            this IQueryable<Order> queryable,
            OrderSortingParameters? parameters
        )
        {
            if (parameters == null)
            {
                parameters = new OrderSortingParameters();
            }
            return parameters.SortingAttribute switch
            {
                OrderSortingAttribute.Number => new Func<
                        IQueryable<Order>,
                        IOrderedQueryable<Order>
                    >(
                        (IQueryable<Order> orders) =>
                        {
                            return parameters?.SortingType is SortingType.Descending
                                ? orders.OrderByDescending(x => x.Number)
                                : orders.OrderBy(x => x.Number);
                        }
                    )(queryable),
                OrderSortingAttribute.CreationDate or _ => new Func<
                        IQueryable<Order>,
                        IOrderedQueryable<Order>
                    >(
                        (IQueryable<Order> orders) =>
                        {
                            return parameters?.SortingType is SortingType.Descending
                                ? orders.OrderByDescending(x => x.CreatedAt)
                                : orders.OrderBy(x => x.CreatedAt);
                        }
                    )(queryable)
            };
        }

        public static IQueryable<Order> FilterNumber(
            this IQueryable<Order> queryable,
            string? number
        )
        {
            if (number == null)
            {
                return queryable;
            }

            return queryable.Where(
                x => EF.Functions.Like(x.Number.ToLower(), $"%{number.ToLower()}%")
            );
        }

        public static IQueryable<Order> FilterdDate(
            this IQueryable<Order> queryable,
            DateTime? start,
            DateTime? end
        )
        {
            if (start != null)
            {
                queryable = queryable.Where(x => x.CreatedAt > start);
            }

            if (end != null)
            {
                queryable = queryable.Where(x => x.CreatedAt < end);
            }

            return queryable;
        }
    }
}
