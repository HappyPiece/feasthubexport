﻿using FeastHub.Backend.BL.Services.Internal;
using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FeastHubBL.Services
{
    public partial class OrderService : IOrderService
    {
        private static NotificationDTO GenerateOrderStatusNotification(Order order)
        {
            return new NotificationDTO
            {
                AddresseeId = order.CustomerId,
                Message = $"Your order's {order.Number} status has changed to {order.Status}"
            };
        }
        private static OperationResult TrySetOrderStatus(Order order, Guid userId, OrderStatus status, bool force = false)
        {
            if (status - order.Status != 1 && !force)
            {
                return new OperationResult(new InvalidOperationException($"User '{userId}' " +
                    $"cannot set status of the order '{order.Id} to '{status}', " +
                    $"because it's current status is '{order.Status}'"));
            }

            order.Status = status;
            return new OperationResult();
        }

        private async Task<OperationResult<Order>> TryGetOrder(Guid orderId)
        {
            var order = await _dbContext.Orders.Where(x => x.Id == orderId).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult<Order>(NotFoundException.GenerateDefault<Order>(orderId));
            }
            return new OperationResult<Order>(order);
        }

        public async Task<OperationResult> TakeToKitchen(Guid orderId, Guid cookId)
        {
            var order = await _dbContext.Orders.Where(x => x.Id == orderId).Include(x => x.Restaurant).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Order>(orderId));
            }

            if (!order.Restaurant.Cooks.Any(x => x == cookId))
            {
                return new OperationResult(new AccessDeniedException($"Cook '{cookId}' cannot be assigned to order '{orderId}' because they do not work in restaurant '{order.Restaurant.Id}'"));
            }

            var result = TrySetOrderStatus(order, cookId, OrderStatus.Cooking);
            if (result.IsFailed)
            {
                return result;
            }
            order.CookId = cookId;

            await _dbContext.SaveChangesAsync();
            _notificationProducerService.SendNotification(GenerateOrderStatusNotification(order));
            return new OperationResult();
        }
        public async Task<OperationResult> PutForCourier(Guid orderId, Guid cookId)
        {
            var order = await _dbContext.Orders.Where(x => x.Id == orderId).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Order>(orderId));
            }

            if (order.CookId != cookId)
            {
                return new OperationResult(new AccessDeniedException($"Cook '{cookId}' cannot set status of the order '{orderId}' " +
                    $"because they are not assigned to it"));
            }

            var result = TrySetOrderStatus(order, cookId, OrderStatus.Packaged);
            if (result.IsFailed)
            {
                return result;
            }

            await _dbContext.SaveChangesAsync();
            _notificationProducerService.SendNotification(GenerateOrderStatusNotification(order));
            return new OperationResult();
        }
        public async Task<OperationResult> TakeForDelivery(Guid orderId, Guid courierId)
        {
            var order = await _dbContext.Orders.Where(x => x.Id == orderId).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Order>(orderId));
            }

            var result = TrySetOrderStatus(order, courierId, OrderStatus.Delivering);
            if (result.IsFailed)
            {
                return result;
            }
            order.CourierId = courierId;

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }
        public async Task<OperationResult> MarkAsDone(Guid orderId, Guid courierId)
        {
            var order = await _dbContext.Orders.Where(x => x.Id == orderId).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Order>(orderId));
            }

            if (order.CookId != courierId)
            {
                return new OperationResult(new AccessDeniedException($"Courier '{courierId}' cannot set status of the order '{orderId}' " +
                    $"because they are not assigned to it"));
            }

            var result = TrySetOrderStatus(order, courierId, OrderStatus.Done);
            if (result.IsFailed)
            {
                return result;
            }

            await _dbContext.SaveChangesAsync();
            _notificationProducerService.SendNotification(GenerateOrderStatusNotification(order));
            return new OperationResult();
        }
        public async Task<OperationResult> Cancel(Guid orderId, Guid userId)
        {
            var orderResult = await TryGetOrder(orderId);
            if (orderResult.IsFailed)
            {
                return orderResult;
            }
            var order = orderResult.Value!;

            if (!(order.Status == OrderStatus.Created && order.CustomerId == userId
                || order.Status == OrderStatus.Created && order.Restaurant.Cooks.Any(x => x == userId)
                || order.Status == OrderStatus.Delivering && order.CourierId == userId))
            {
                return new OperationResult(new AccessDeniedException($"User '{userId}' cannot cancel order '{orderId}'"));
            }

            order.Status = OrderStatus.Canceled;
            await _dbContext.SaveChangesAsync();
            _notificationProducerService.SendNotification(GenerateOrderStatusNotification(order));
            return new OperationResult();
        }
    }
}
