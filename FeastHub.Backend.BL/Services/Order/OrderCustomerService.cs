﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace FeastHubBL.Services
{
    public partial class OrderService : IOrderService
    {
        public async Task<OperationResult> CreateOrder(Guid customerId, CreateOrderDTO dto)
        {
            var cart = await _internalService.GetCartEntity(customerId);
            var restaurantIds = new HashSet<Guid>();
            cart.Piles.ForEach(p => restaurantIds.Add(p.Dish.RestaurantId));

            var restaurants = await _dbContext.Restaurants.Where(x => restaurantIds.Contains(x.Id)).Include(x => x.Orders).ToListAsync();

            var orders = new List<Order>();

            foreach (var restaurant in restaurants)
            {
                var piles = cart.Piles.Where(x => x.Dish.RestaurantId == restaurant.Id && x.Dish.IsAccessible).ToList();

                if (piles.Any())
                {
                    piles.ForEach(x => x.ArchivePricePerUnit = x.Dish.Price);

                    var order = new Order
                    {
                        Address = dto.Address,
                        CustomerId = customerId,
                        Cart = new Cart
                        {
                            Piles = piles,
                            CustomerId = customerId
                        },
                        Status = OrderStatus.Created,
                        Restaurant = restaurant
                    };

                    orders.Add(order);
                }
            }

            if (!orders.Any())
            {
                return new OperationResult(new InvalidOperationException("Cannot create order: cart is empty"));
            }

            await _dbContext.Orders.AddRangeAsync(orders);
            orders.ForEach(x => _dbContext.Entry(x).State = EntityState.Added);
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> CopyOrderContent(Guid customerId, Guid orderId)
        {
            var order = await _dbContext.Orders
                .Include(x => x.Cart).ThenInclude(x => x.Piles).ThenInclude(x => x.Dish).ThenInclude(x => x.Menus)
                .Where(x => x.Id == orderId && x.CustomerId == customerId).SingleOrDefaultAsync();
            if (order == null)
            {
                return new OperationResult<Order>(NotFoundException.GenerateDefault<Order>(orderId));
            }

            var cart = await _internalService.GetCartEntity(customerId);

            foreach (var pile in order.Cart.Piles.Where(x => x.Dish.IsAccessible))
            {
                if (cart.Piles.Any(x => x.Dish.Id == pile.Dish.Id))
                {
                    var cartPile = cart.Piles.Where(x => x.Dish.Id == pile.Dish.Id).SingleOrDefault();
                    cartPile.Count = pile.Count; 
                }
                else
                {
                    var newPile = new DishPile
                    {
                        Dish = pile.Dish,
                        Count = pile.Count
                    };
                    cart.Piles.Add(newPile);
                    _dbContext.Entry(newPile).State = EntityState.Added;
                }
            }
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult<CartDTO>> GetCart(Guid customerId)
        {
            var cart = await _internalService.GetCartEntity(customerId);

            return new OperationResult<CartDTO>((CartDTO)cart);
        }

        public async Task<OperationResult> SetDishNumberInCart(Guid customerId, Guid dishId, int number)
        {
            var dishResult = await _internalService.GetDishEntity(dishId);
            if (dishResult.IsFailed)
            {
                return dishResult;
            }
            var dish = dishResult.Value!;

            await _dbContext.Entry(dish).Collection(x => x.Menus).LoadAsync();

            var cart = await _internalService.GetCartEntity(customerId);

            if (cart.Piles.Any(x => x.Dish.Id == dishId))
            {
                var pile = cart.Piles.Where(x => x.Dish.Id == dishId).SingleOrDefault();
                if (number != 0)
                {
                    if (dish.IsAccessible)
                    {
                        pile.Count = number;
                    }
                    else
                    {
                        return new OperationResult(new InvalidOperationException($"Dish '{dishId}' does not exist or is unaccessible"));
                    }
                }
                else
                {
                    cart.Piles.Remove(pile);
                }
            }
            else
            {
                if (!dish.IsAccessible)
                {
                    return new OperationResult(new InvalidOperationException($"Dish '{dishId}' does not exist or is unaccessible"));
                }

                var pile = new DishPile
                {
                    Dish = dish,
                    Count = number
                };
                cart.Piles.Add(pile);
                _dbContext.Entry(pile).State = EntityState.Added;
            }


            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }
    }
}
