﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FeastHubBL.Services
{
    public class NotificationService : INotificationProducerService
    {
        private readonly RabbitMQOptions _options;
        private readonly IConnection _connection;

        public NotificationService(IConnection connection, IOptions<RabbitMQOptions> options)
        {
            _options = options.Value;
            _connection = connection;
        }

        public void SendNotification(NotificationDTO dto)
        {
            var message = JsonSerializer.Serialize(dto);
            using (var channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: _options.QueueName,
                       durable: false,
                       exclusive: false,
                       autoDelete: false,
                       arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                       routingKey: _options.QueueName,
                       basicProperties: null,
                       body: body);
            }
        }
    }
}
