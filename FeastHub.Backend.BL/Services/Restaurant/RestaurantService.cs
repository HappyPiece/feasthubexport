﻿using FeastHub.Backend.BL.Common;
using FeastHub.Backend.BL.Services.Internal;
using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace FeastHub.BL.Services
{
    public partial class RestaurantService : IRestaurantService
    {
        private readonly FeastHubBackendDbContext _dbContext;
        private readonly IInternalService _internalService;
        private readonly PagedViewOptions _pagedViewOptions;

        public RestaurantService(
            FeastHubBackendDbContext dbContext,
            IInternalService internalService,
            IOptions<PagedViewOptions> pagedViewOptions
        )
        {
            _dbContext = dbContext;
            _internalService = internalService;
            _pagedViewOptions = pagedViewOptions.Value;
        }

        public async Task<OperationResult> CanManage(Guid restaurantId, Guid userId)
        {
            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == restaurantId)
                .SingleOrDefaultAsync();
            if (restaurant == null)
            {
                return new OperationResult(
                    NotFoundException.GenerateDefault<Restaurant>(restaurantId)
                );
            }
            if (!restaurant.Managers.Where(x => x == userId).Any())
            {
                return new OperationResult(
                    new AccessDeniedException(
                        $"User '{userId}' can not manage restaurant '{restaurantId}'"
                    )
                );
            }
            return new OperationResult();
        }

        public async Task<OperationResult<RestaurantDTO>> GetRestaurant(Guid id)
        {
            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == id)
                .Include(x => x.Menus)
                .ThenInclude(x => x.Dishes)
                .Include(x => x.Categories)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Ratings)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Categories)
                .SingleOrDefaultAsync();

            if (restaurant == null)
            {
                return new OperationResult<RestaurantDTO>(
                    NotFoundException.GenerateDefault<Restaurant>(id)
                );
            }
            return new OperationResult<RestaurantDTO>((RestaurantDTO)restaurant);
        }

        public async Task<List<RestaurantShortDTO>> GetRestaurants()
        {
            var restaurants = await _dbContext.Restaurants
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Ratings)
                .ToListAsync();
            return restaurants.Select(x => (RestaurantShortDTO)x).ToList();
        }

        public async Task<OperationResult<PagedView<RestaurantShortDTO>>> GetRestaurantsPaged(
            RestaurantQueryParameters query
        )
        {
            var restaurantsQuery = _dbContext.Restaurants
                .FilterName(query.Name)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Ratings);

            var count = await restaurantsQuery.CountAsync();

            if (
                !PagedViewUtilities.PageExists(
                    query.Page,
                    count,
                    _pagedViewOptions.RestaurantPageSize
                )
            )
            {
                return new OperationResult<PagedView<RestaurantShortDTO>>(
                    new NotFoundException("Requested page does not exist")
                );
            }

            var restaurants = await restaurantsQuery
                .SortRestaurants(query.SortingParameters)
                .Skip((query.Page - 1) * _pagedViewOptions.RestaurantPageSize)
                .Take(_pagedViewOptions.RestaurantPageSize)
                .ToListAsync();

            var result = new PagedView<RestaurantShortDTO>
            {
                Items = restaurants.Select(x => (RestaurantShortDTO)x).ToList(),
                Info = new PageInfo
                {
                    CurrentPage = query.Page,
                    PageCount = PagedViewUtilities.CountPages(
                        count,
                        _pagedViewOptions.RestaurantPageSize
                    ),
                    PageSize = restaurants.Count
                }
            };

            return new OperationResult<PagedView<RestaurantShortDTO>>(result);
        }

        public async Task<OperationResult> CreateRestaurant(CreateRestaurantDTO dto)
        {
            if (await _dbContext.Restaurants.AnyAsync(x => x.Name == dto.Name))
            {
                return new OperationResult(
                    new AlreadyExistsException($"Restaurant with name {dto.Name} already exists")
                );
            }

            await _dbContext.Restaurants.AddAsync(new Restaurant { Name = dto.Name });
            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }
    }
}
