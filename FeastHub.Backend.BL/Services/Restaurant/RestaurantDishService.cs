﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common.DTO;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Common.DTO.View;
using Microsoft.IdentityModel.Tokens;
using FeastHub.Common.Configurations;
using static FeastHub.Backend.DAL.Models.Dish;
using FeastHubBL.Services;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Components;
using FeastHub.Common.DTO.View.QueryParameters;

namespace FeastHub.BL.Services
{
    public partial class RestaurantService : IRestaurantService
    {
        public async Task<OperationResult> CanManageDish(Guid dishId, Guid userId)
        {
            var restaurant = await _dbContext.Dishes
                .Where(x => x.Id == dishId)
                .Select(x => x.Restaurant)
                .SingleOrDefaultAsync();
            if (restaurant == null)
            {
                return new OperationResult(
                    NotFoundException.GenerateDefault<Restaurant>(restaurant.Id)
                );
            }
            if (!restaurant.Managers.Where(x => x == userId).Any())
            {
                return new OperationResult(
                    new AccessDeniedException(
                        $"User '{userId}' can not manage restaurant '{restaurant.Id}'"
                    )
                );
            }
            return new OperationResult();
        }

        public async Task<OperationResult<PagedView<DishDTO>>> GetRestaurantDishes(
            Guid restaurantId,
            DishQueryParameters query
        )
        {
            var restaurant = await _dbContext.Restaurants
                .Include(x => x.Categories)
                .Include(x => x.Menus)
                .SingleOrDefaultAsync(x => x.Id == restaurantId);

            if (restaurant == null)
            {
                return new OperationResult<PagedView<DishDTO>>(
                    NotFoundException.GenerateDefault<Restaurant>(restaurantId)
                );
            }

            if (
                !restaurant.CategoriesExist<PagedView<DishDTO>>(
                    query.Categories,
                    out var categoriesCheckResult
                )
            )
            {
                return categoriesCheckResult;
            }

            if (!restaurant.MenusExist<PagedView<DishDTO>>(query.MenuIds, out var menusCheckResult))
            {
                return menusCheckResult;
            }

            var dishesQuery = _dbContext.Dishes
                .Where(x => x.RestaurantId == restaurantId)
                .Include(x => x.Ratings)
                .Include(x => x.Categories)
                .FilterAccessible()
                .FilterVegeterian(query.IsVegeterian)
                .FilterCategory(query.Categories)
                .FilterMenu(query.MenuIds);

            var count = await dishesQuery.CountAsync();

            if (!PagedViewUtilities.PageExists(query.Page, count, _pagedViewOptions.DishPageSize))
            {
                return new OperationResult<PagedView<DishDTO>>(
                    new NotFoundException("Requested page does not exist")
                );
            }

            var dishes = await dishesQuery
                .Sort(query.SortingParameters)
                .Skip((query.Page - 1) * _pagedViewOptions.DishPageSize)
                .Take(_pagedViewOptions.DishPageSize)
                .ToListAsync();

            var result = new PagedView<DishDTO>
            {
                Items = dishes.Select(x => (DishDTO)x).ToList(),
                Info = new PageInfo
                {
                    CurrentPage = query.Page,
                    PageCount = PagedViewUtilities.CountPages(
                        count,
                        _pagedViewOptions.DishPageSize
                    ),
                    PageSize = dishes.Count
                }
            };

            return new OperationResult<PagedView<DishDTO>>(result);
        }

        private async Task<OperationResult<List<Dish>>> TryGetDishes(
            Guid restaurantId,
            List<Guid> dishIds
        )
        {
            var dishesQuery = _dbContext.Dishes
                .Where(x => x.RestaurantId == restaurantId)
                .Where(x => dishIds.Contains(x.Id));
            var dishes = await dishesQuery.ToListAsync();

            if (dishes.Count != dishIds.Count)
            {
                foreach (var dish in dishIds)
                {
                    if (!dishes.Any(x => x.Id == dish))
                    {
                        return new OperationResult<List<Dish>>(
                            new NotFoundException(
                                $"There is no dish with '{dish}' in restaurant '{restaurantId}'"
                            )
                        );
                    }
                }
            }

            return new OperationResult<List<Dish>>(dishes);
        }
    }

    public static class IQueryableDishExtension
    {
        public static IQueryable<Dish> FilterAccessible(this IQueryable<Dish> queryable)
        {
            return queryable.Where(x => x.IsAccessible);
        }

        public static IQueryable<Dish> FilterVegeterian(
            this IQueryable<Dish> queryable,
            bool? isVegeterian
        )
        {
            if (isVegeterian == null)
            {
                return queryable;
            }

            return queryable.Where(x => x.IsVegeterian.HasValue && x.IsVegeterian == isVegeterian);
        }

        public static IQueryable<Dish> FilterCategory(
            this IQueryable<Dish> queryable,
            List<string> categories
        )
        {
            if (categories.IsNullOrEmpty())
            {
                return queryable;
            }

            return queryable.Where(x => x.Categories.Any(c => categories.Contains(c.Name)));
        }

        public static IQueryable<Dish> FilterMenu(
            this IQueryable<Dish> queryable,
            List<Guid> menuIds
        )
        {
            if (menuIds.IsNullOrEmpty())
            {
                return queryable;
            }

            return queryable.Where(x => x.Menus.Any(m => menuIds.Contains(m.Id)));
        }

        public static IOrderedQueryable<Dish> Sort(
            this IQueryable<Dish> queryable,
            DishSortingParameters parameters
        )
        {
            return parameters.SortingAttribute switch
            {
                DishSortingAttribute.Price => new Func<IQueryable<Dish>, IOrderedQueryable<Dish>>(
                        (IQueryable<Dish> dishes) =>
                        {
                            return parameters?.SortingType is SortingType.Descending
                                ? dishes.OrderByDescending(x => x.Price)
                                : dishes.OrderBy(x => x.Price);
                        }
                    )(queryable),

                DishSortingAttribute.Rating => new Func<IQueryable<Dish>, IOrderedQueryable<Dish>>(
                        (IQueryable<Dish> dishes) =>
                        {
                            return parameters?.SortingType is SortingType.Descending
                                ? dishes.OrderByDescending(x => x.Rating)
                                : dishes.OrderBy(x => x.Rating);
                        }
                    )(queryable),

                _ => new Func<IQueryable<Dish>, IOrderedQueryable<Dish>>(
                        (IQueryable<Dish> dishes) =>
                        {
                            return parameters?.SortingType is SortingType.Descending
                                ? dishes.OrderByDescending(x => x.Name)
                                : dishes.OrderBy(x => x.Name);
                        }
                    )(queryable),
            };
        }
    }
}
