﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common.DTO;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Common.DTO.View;
using Microsoft.IdentityModel.Tokens;
using FeastHub.Common.Configurations;
using static FeastHub.Backend.DAL.Models.Dish;
using FeastHubBL.Services;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Components;

namespace FeastHub.BL.Services
{
    public partial class RestaurantService : IRestaurantService
    {
        public async Task<OperationResult> CreateCategories(Guid restaurantId, List<string> categories)
        {
            var restaurantResult = await _internalService.GetRestaurantEntity(restaurantId);
            if (restaurantResult.IsFailed)
            {
                return restaurantResult;
            }
            var restaurant = restaurantResult.Value!;
            await _dbContext.Entry(restaurant).Collection(x => x.Categories).LoadAsync();

            foreach (var category in categories)
            {
                if (restaurant.Categories.Any(x => x.Name == category))
                {
                    return new OperationResult(new AlreadyExistsException($"There is already a category called '{category}' in restaurant '{restaurantId}'"));
                }
                restaurant.Categories.Add(new DishCategory { Name = category });
            }

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> EditCategories(Guid restaurantId, Dictionary<string, string> categories)
        {
            var restaurantResult = await _internalService.GetRestaurantEntity(restaurantId);
            if (restaurantResult.IsFailed)
            {
                return restaurantResult;
            }
            var restaurant = restaurantResult.Value!;
            await _dbContext.Entry(restaurant).Collection(x => x.Categories).LoadAsync();

            if (!restaurant.CategoriesExist(categories.Keys.ToList(), out var categoriesCheckResult))
            {
                return categoriesCheckResult;
            }

            foreach (var category in categories)
            {
                if (restaurant.Categories.Any(x => x.Name == category.Value))
                {
                    return new OperationResult(new AlreadyExistsException($"There is already a category called '{category.Value}' in restaurant '{restaurantId}'"));
                }
                restaurant.Categories.Single(x => x.Name == category.Key).Name = category.Value;
            }

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> DeleteCategories(Guid restaurantId, List<string> categories)
        {
            var restaurantResult = await _internalService.GetRestaurantEntity(restaurantId);
            if (restaurantResult.IsFailed)
            {
                return restaurantResult;
            }
            var restaurant = restaurantResult.Value!;
            await _dbContext.Entry(restaurant).Collection(x => x.Categories).LoadAsync();

            if (!restaurant.CategoriesExist(categories, out var categoriesCheckResult))
            {
                return categoriesCheckResult;
            }

            foreach (var category in categories)
            {
                restaurant.Categories.RemoveAll(x => x.Name == category);
            }

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        } 
    }
}
