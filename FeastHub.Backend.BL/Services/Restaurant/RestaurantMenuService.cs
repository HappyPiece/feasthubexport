﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace FeastHub.BL.Services
{
    public partial class RestaurantService : IRestaurantService
    {

        public async Task<OperationResult> CanManageMenu(Guid menuId, Guid userId)
        {
            var restaurant = await _dbContext.Menus.Where(x => x.Id == menuId).Select(x => x.Restaurant).SingleOrDefaultAsync();
            if (restaurant == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Restaurant>(restaurant.Id));
            }
            if (!restaurant.Managers.Where(x => x == userId).Any())
            {
                return new OperationResult(new AccessDeniedException($"User '{userId}' can not manage restaurant '{restaurant.Id}'"));
            }
            return new OperationResult();
        }

        public async Task<OperationResult> CreateMenu(Guid restaurantId, CreateMenuDTO dto)
        {
            var restaurantResult = await _internalService.GetRestaurantEntity(restaurantId);
            if (restaurantResult.IsFailed)
            {
                return restaurantResult;
            }
            var restaurant = restaurantResult.Value!;

            if (restaurant.Menus.Any(x => x.Name == dto.Name))
            {
                return new OperationResult(new AlreadyExistsException($"There is already a menu called '{dto.Name}' in restaurant '{restaurantId}'"));
            }

            var dishesResult = await TryGetDishes(restaurantId, dto.Dishes);
            if (dishesResult.IsFailed)
            {
                return dishesResult;
            }

            var menu = new Menu
            {
                Name = dto.Name,
                Description = dto.Description,
                PhotoUrl = dto.PhotoUrl,
                IsActive = dto.IsActive,
                Dishes = dishesResult.Value!
            };

            restaurant.Menus.Add(menu);
            _dbContext.Entry(menu).State = EntityState.Added;
            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> EditMenu(Guid menuId, EditMenuDTO dto)
        {
            var menuResult = await TryGetMenu(menuId);
            if (menuResult.IsFailed)
            {
                return menuResult;
            }
            var menu = menuResult.Value!;

            if (dto.Name != null && await _dbContext.Menus.Where(x => x.Name == dto.Name && x.RestaurantId == menu.RestaurantId).AnyAsync())
            {
                return new OperationResult(new AlreadyExistsException($"There is already a menu called '{dto.Name}' in restaurant '{menu.RestaurantId}'"));
            }

            {
                menu.Name = dto.Name ?? menu.Name;
                menu.Description = dto.Description;
                menu.PhotoUrl = dto.PhotoUrl;
                menu.IsActive = dto.IsActive ?? menu.IsActive;
            }

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> DeleteMenu(Guid menuId)
        {
            var menuResult = await TryGetMenu(menuId);
            if (menuResult.IsFailed)
            {
                return menuResult;
            }
            var menu = menuResult.Value!;
            await _dbContext.Entry(menu).Collection(x => x.Dishes).LoadAsync();


            _dbContext.Remove(menu);

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> AddDishesToMenu(Guid menuId, List<Guid> dishIds)
        {
            var menuResult = await TryGetMenu(menuId);
            if (menuResult.IsFailed)
            {
                return menuResult;
            }
            var menu = menuResult.Value!;

            var dishesResult = await TryGetDishes(menu.RestaurantId, dishIds);
            if (dishesResult.IsFailed)
            {
                return dishesResult;
            }
            var dishes = dishesResult.Value!;

            var presentDishes = await _dbContext.Dishes.Where(x => x.Menus.Contains(menu) && dishIds.Contains(x.Id)).ToListAsync();
            if (presentDishes.Count > 0)
            {
                return new OperationResult(new AlreadyExistsException($"There is already a dish '{presentDishes.First().Id}' in menu '{menuId}'"));
            }

            menu.Dishes.AddRange(dishes);

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> RemoveDishesFromMenu(Guid menuId, List<Guid> dishIds)
        {
            var menuResult = await TryGetMenu(menuId);
            if (menuResult.IsFailed)
            {
                return menuResult;
            }
            var menu = menuResult.Value!;

            var dishesResult = await TryGetDishes(menu.RestaurantId, dishIds);
            if (dishesResult.IsFailed)
            {
                return dishesResult;
            }
            var dishes = dishesResult.Value!;

            var presentDishesQuery = _dbContext.Dishes.Where(x => x.Menus.Contains(menu) && dishIds.Contains(x.Id));

            Console.WriteLine(presentDishesQuery.ToQueryString());

            var presentDishes = await presentDishesQuery.ToListAsync();

            if (presentDishes.Count < dishIds.Count)
            {
                foreach (var dishId in dishIds)
                {
                    if (!presentDishes.Any(x => x.Id == dishId))
                    {
                        return new OperationResult(new NotFoundException($"There is no dish '{dishId}' in menu '{menuId}'"));
                    }
                }
            }

            await _dbContext.Entry(menu).Collection(x => x.Dishes).LoadAsync();

            menu.Dishes.RemoveAll(x => dishes.Contains(x));

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        private async Task<OperationResult<Menu>> TryGetMenu(Guid menuId)
        {
            var menu = await _dbContext.Menus
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Menus)
                .Where(x => x.Id == menuId)
                .SingleOrDefaultAsync();
            if (menu == null)
            {
                return new OperationResult<Menu>(NotFoundException.GenerateDefault<Menu>(menuId));
            }
            return new OperationResult<Menu>(menu);
        }
    }
}
