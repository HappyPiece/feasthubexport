﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHubBL.Services
{
    public class RatingService : IRatingService
    {
        private readonly FeastHubBackendDbContext _dbContext;

        public RatingService(FeastHubBackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<OperationResult> CreateDishRating(Guid dishId, Guid customerId, CreateRatingDTO dto)
        {
            var dish = await _dbContext.Dishes
                .Include(x => x.Restaurant).ThenInclude(x => x.Dishes)
                .Where(x => x.Id == dishId).SingleOrDefaultAsync();

            if (dish == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Dish>(dishId));
            }

            if (!await _dbContext.Orders.Where(x => x.CustomerId == customerId 
            && x.Status == OrderStatus.Done
            && x.Cart.Piles.Where(p => p.Dish.Id == dishId).Any()).AnyAsync())
            {
                return new OperationResult(new AccessDeniedException($"User '{customerId}' can not rate dish '{dish}'"));
            }

            if (await _dbContext.Ratings.AnyAsync(x => x.CustomerId == customerId && x.DishId == dishId))
            {
                return new OperationResult(new AlreadyExistsException($"The user '{customerId}' have already set a rating for a dish '{dishId}'. Try editing it instead"));
            }

            var rating = new Rating
            {
                CustomerId = customerId,
                Score = dto.Score,
                Dish = dish
            };

            dish.Ratings.Add(rating);

            _dbContext.Entry(rating).State = EntityState.Added;

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> EditDishRating(Guid dishId, Guid customerId, EditRatingDTO dto)
        {
            var rating = await _dbContext.Ratings
                .Where(x => x.DishId == dishId && x.CustomerId == customerId)
                .Include(x => x.Dish).ThenInclude(x => x.Restaurant).ThenInclude(x => x.Dishes)
                .SingleOrDefaultAsync();

            if (rating == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Rating>());
            }

            rating.Score = dto.Score;
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> DeleteDishRating(Guid dishId, Guid customerId)
        {
            var rating = await _dbContext.Ratings
                .Where(x => x.DishId == dishId && x.CustomerId == customerId)
                .Include(x => x.Dish).ThenInclude(x => x.Restaurant).ThenInclude(x => x.Dishes)
                .SingleOrDefaultAsync();

            if (rating == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Rating>());
            }

            _dbContext.Ratings.Remove(rating);
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }
    }
}
