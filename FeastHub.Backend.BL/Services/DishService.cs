﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static FeastHub.Backend.DAL.Models.Dish;

namespace FeastHubBL.Services
{
    public class DishService : IDishService
    {
        private readonly FeastHubBackendDbContext _dbContext;

        public DishService(FeastHubBackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<OperationResult> CreateDish(Guid restaurantId, CreateDishDTO dto)
        {
            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == restaurantId)
                .Include(x => x.Dishes)
                .Include(x => x.Categories)
                .SingleOrDefaultAsync();

            if (restaurant == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Restaurant>(restaurantId));
            }

            if (restaurant.Dishes.Any(x => x.Name == dto.Name))
            {
                return new OperationResult(new AlreadyExistsException($"There is already a dish named '{dto.Name}' in restaurant '{restaurantId}'"));
            }

            if (!restaurant.CategoriesExist(dto.Categories, out var categoriesCheckResult))
            {
                return categoriesCheckResult;
            }

            var dish = new Dish
            {
                Name = dto.Name,
                IsVegeterian = dto.IsVegeterian,
                Description = dto.Description,
                PhotoUrl = dto.PhotoUrl,
                Price = dto.Price,
                Categories = restaurant.GetCategoriesByName(dto.Categories)
            };

            restaurant.Dishes.Add(dish);

            _dbContext.Entry(dish).State = EntityState.Added;
            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> DeleteDish(Guid dishId)
        {
            var dish = await _dbContext.Dishes.Include(x => x.Restaurant).Where(x => x.Id == dishId).SingleOrDefaultAsync();

            if (dish == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Dish>(dishId));
            }

            _dbContext.Dishes.Remove(dish);
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> EditDish(Guid dishId, EditDishDTO dto)
        {
            var dish = await _dbContext.Dishes
                .Where(x => x.Id == dishId)
                .Include(x => x.Categories)
                .Include(x => x.Restaurant).ThenInclude(x => x.Categories)
                .SingleOrDefaultAsync();

            if (dish == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<Dish>(dishId));
            }

            if (dish.Restaurant.CategoriesExist(dto.Categories, out var categoriesCheckResult))
            {
                return categoriesCheckResult;
            }

            if (dto.Name != null && dto.Name != dish.Name && await _dbContext.Dishes.AnyAsync(x => x.RestaurantId == dish.RestaurantId && x.Name == dto.Name))
            {
                return new OperationResult(new AlreadyExistsException($"There is already a dish named '{dto.Name}' in restaurant '{dish.RestaurantId}'"));
            }

            {
                dish.Name = dto.Name ?? dish.Name;
                dish.Price = dto.Price ?? dish.Price;
                dish.Description = dto.Description;
                dish.PhotoUrl = dto.PhotoUrl;
                dish.IsVegeterian = dto.IsVegeterian;
                dish.Categories = dish.Restaurant.GetCategoriesByName(dto.Categories);
            }

            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public Task<OperationResult<DishDTO>> GetDish(Guid dishId)
        {
            throw new NotImplementedException();
        }
    }

    public static class RestaurantExtensions
    {
        public static List<DishCategory> GetCategoriesByName(this Restaurant restaurant, List<string> categories)
        {
            return restaurant.Categories.Where(x => categories.Any(c => c == x.Name)).ToList();
        }
        public static bool CategoriesExist<T>(this Restaurant restaurant, List<string> categories, out OperationResult<T> result)
        {
            if (restaurant.CategoriesExist(categories, out OperationResult res))
            {
                result = new OperationResult<T>();
                return true;
            }
            else
            {
                result = new OperationResult<T>(res.Exception!);
                return false;
            }
        }
        public static bool CategoriesExist(this Restaurant restaurant, List<string> categories, out OperationResult result)
        {
            foreach (var category in categories)
            {
                if (!restaurant.Categories.Any(x => x.Name == category))
                {
                    result = new OperationResult(new NotFoundException($"There is no category named '{category}' in restaurant '{restaurant.Id}'"));
                    return false;
                }
            }
            result = new OperationResult();
            return true;
        }
        public static bool MenusExist<T>(this Restaurant restaurant, List<Guid> menuIds, out OperationResult<T> result)
        {
            if (restaurant.MenusExist(menuIds, out OperationResult res))
            {
                result = new OperationResult<T>();
                return true;
            }
            else
            {
                result = new OperationResult<T>(res.Exception!);
                return false;
            }
        }
        public static bool MenusExist(this Restaurant restaurant, List<Guid> menuIds, out OperationResult result)
        {
            foreach (var id in menuIds)
            {
                if (!restaurant.Menus.Any(x => x.Id == id))
                {
                    result = new OperationResult(new NotFoundException($"There is no menu '{id}' in restaurant '{restaurant.Id}'"));
                    return false;
                }
            }
            result = new OperationResult();
            return true;
        }
    }
}
