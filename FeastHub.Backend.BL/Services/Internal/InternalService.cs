﻿using FeastHub.Backend.DAL;
using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using FeastHub.Common.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.BL.Services.Internal
{
    public class InternalService : IInternalService
    {
        private readonly FeastHubBackendDbContext _dbContext;
        public InternalService(FeastHubBackendDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<OperationResult<Dish>> GetDishEntity(Guid id)
        {
            var dish = await _dbContext.Dishes.Where(x => x.Id == id).SingleOrDefaultAsync();
            if (dish == null)
            {
                return new OperationResult<Dish>(NotFoundException.GenerateDefault<Dish>(id));
            }
            return new OperationResult<Dish>(dish);
        }

        public async Task<OperationResult<Restaurant>> GetRestaurantEntity(Guid id)
        {
            var restaurant = await _dbContext.Restaurants.Where(x => x.Id == id).SingleOrDefaultAsync();
            if (restaurant == null)
            {
                return new OperationResult<Restaurant>(NotFoundException.GenerateDefault<Restaurant>(id));
            }
            return new OperationResult<Restaurant>(restaurant);
        }

        public async Task<Cart> GetCartEntity(Guid customerId)
        {
            var cart = await _dbContext.Carts
               .Where(x => x.CustomerId == customerId && x.OrderId == null)
               .Include(x => x.Piles).ThenInclude(x => x.Dish).ThenInclude(x => x.Menus)
               .SingleOrDefaultAsync();

            if (cart == null)
            {
                cart = new Cart
                {
                    CustomerId = customerId
                };
                _dbContext.Carts.Add(cart);
            }

            return cart;
        }
    }
}
