﻿using FeastHub.Backend.DAL.Models;
using FeastHub.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Backend.BL.Services.Internal
{
    public interface IInternalService
    {
        Task<OperationResult<Dish>> GetDishEntity(Guid id);
        Task<OperationResult<Restaurant>> GetRestaurantEntity(Guid id);
        public Task<Cart> GetCartEntity(Guid customerId);
    }
}
