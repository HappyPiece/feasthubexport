﻿using FeastHub.Backend.DAL;
using FeastHub.BL.Services;
using FeastHubBL.Services;
using FeastHub.Common.Contracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Backend.DAL.Models;
using FeastHub.Backend.BL.Services.Internal;
using FeastHub.Common.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using FeastHub.Common.Configurations;

namespace FeastHub.Backend.BL
{
    public static class BackendBLExtension
    {
        public static IServiceCollection AddBackendBL(this IServiceCollection services, string dbConnectionString, RabbitMQOptions options)
        {
            services.AddBackendDAL(dbConnectionString);
            services.AddScoped<IInternalService, InternalService>();
            services.AddScoped<IRestaurantService, RestaurantService>();
            services.AddScoped<IDishService, DishService>();
            services.AddScoped<IRatingService, RatingService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<INotificationProducerService, NotificationService>();

            services.AddSingleton(service => new ConnectionFactory
            {
                HostName = options.HostName,
                UserName = options.UserName,
                Password = options.Password
            }.CreateConnection());
            return services;
        }

        public static async Task<WebApplication> UseBackendBL(this WebApplication app)
        {
            await app.UseBackendDAL();
            return app;
        }
    }
}
