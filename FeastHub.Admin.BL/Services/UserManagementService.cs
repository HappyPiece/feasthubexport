﻿using FeastHub.Backend.BL.Common;
using FeastHub.Backend.DAL;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using FeastHub.User.BL.Common;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Data;
using System.Security.Claims;

namespace FeastHub.Admin.BL.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly FeastHubBackendDbContext _backendDbContext;
        private readonly FeastHubUserDbContext _userDbContext;
        private readonly UserManager<FeastHubUser> _userManager;
        private readonly PagedViewOptions _pagedViewOptions;

        public UserManagementService(
            FeastHubBackendDbContext backendDbContext,
            UserManager<FeastHubUser> userManager,
            FeastHubUserDbContext userDbContext,
            IOptions<PagedViewOptions> pagedViewOptions
        )
        {
            _backendDbContext = backendDbContext;
            _userDbContext = userDbContext;
            _userManager = userManager;
            _pagedViewOptions = pagedViewOptions.Value;
        }

        public async Task<OperationResult> CreateUser(
            CreateUserWithRolesDTO dto,
            ClaimsPrincipal initiator
        )
        {
            var creatorRoles = initiator.ExtractRoles();
            foreach (var role in dto.Roles)
            {
                if (!creatorRoles.CanAssign(role))
                    return new OperationResult(
                        new AccessDeniedException($"Not enough rights to assign {role}")
                    );
            }

            var user = new FeastHubUser(dto);
            var result = await _userManager.CreateAsync(user, dto.Password);

            if (!result.Succeeded)
            {
                return new OperationResult<TokenPairDTO>(
                    new IdentityException(result.Errors.ToList())
                );
            }

            foreach (var role in dto.Roles)
            {
                result = await _userManager.AddToRoleAsync(user, role.ToString());
                if (!result.Succeeded)
                    return new OperationResult<TokenPairDTO>(
                        new IdentityException(result.Errors.ToList())
                    );
            }

            _userDbContext.Entry(user).State = EntityState.Added;
            _userDbContext.SaveChanges();
            return new OperationResult();
        }

        public async Task<OperationResult> BanUser(
            Guid userId,
            ActionType actionType,
            ClaimsPrincipal initiator
        )
        {
            var creatorRoles = initiator.ExtractRoles();
            var userResult = await GetUserEntity(userId);
            if (userResult.IsFailed)
            {
                return userResult;
            }
            var user = userResult.Value!;
            if (!creatorRoles.CanOperate(user.Roles.Select(x => x.Role.Type)))
            {
                return new OperationResult(
                    new AccessDeniedException($"Not enough rights to ban user {userId}")
                );
            }

            if (actionType == ActionType.Add)
            {
                if (user.IsBanned)
                {
                    return new OperationResult(
                        new InvalidOperationException($"User {userId} is already banned")
                    );
                }
                user.BannedAt = DateTime.UtcNow;
            }
            else
            {
                if (!user.IsBanned)
                {
                    return new OperationResult(
                        new InvalidOperationException($"User {userId} is not banned")
                    );
                }
                user.BannedAt = null;
            }

            await _userDbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> ManageRole(
            Guid userId,
            FeastHubRoleType role,
            ActionType actionType,
            ClaimsPrincipal initiator
        )
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            if (user == null)
            {
                return new OperationResult(NotFoundException.GenerateDefault<FeastHubUser>(userId));
            }
            if (!initiator.ExtractRoles().CanAssign(role))
            {
                return new OperationResult(
                    new AccessDeniedException($"Not enough rights to operate {role}")
                );
            }
            var result =
                actionType == ActionType.Add
                    ? await _userManager.AddToRoleAsync(user, role.ToString())
                    : await _userManager.RemoveFromRoleAsync(user, role.ToString());
            if (!result.Succeeded)
            {
                return new OperationResult(new IdentityException(result.Errors.ToList()));
            }
            await _userDbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> SyncRoles(
            Guid userId,
            List<FeastHubRoleType> roles,
            ClaimsPrincipal initiator
        )
        {
            var userResult = await GetUserEntity(userId);
            if (userResult.IsFailed)
            {
                return new OperationResult(NotFoundException.GenerateDefault<FeastHubUser>(userId));
            }
            var user = userResult.Value!;
            var concurrencyStamp = user.ConcurrencyStamp;

            if (!initiator.ExtractRoles().CanOperate(roles))
            {
                return new OperationResult(
                    new AccessDeniedException($"Not enough rights to manage user '{user.Id}'")
                );
            }
            foreach (var role in roles)
            {
                var roleEntity = await _userDbContext.Roles
                    .Where(x => x.Type == role)
                    .SingleOrDefaultAsync();
                if (!initiator.ExtractRoles().CanAssign(role))
                {
                    return new OperationResult(
                        new AccessDeniedException($"Not enough rights to operate {role}")
                    );
                }
                if (!user.Roles.Any(x => x.Role.Type == role))
                {
                    _userDbContext.UserRoles.Add(
                        new FeastHubUserRole
                        {
                            UserId = user.Id,
                            RoleId =
                                roleEntity?.Id
                                ?? throw new ArgumentNullException(
                                    "It seems that required role has somehow ceased to exist in database"
                                )
                        }
                    );
                }
            }
            foreach (var role in user.Roles.Select(x => x.Role.Type))
            {
                if (!roles.Any(x => x == role))
                {
                    var userRoleEntity = await _userDbContext.UserRoles
                        .Where(x => (x.Role.Type == role) && x.UserId == userId)
                        .SingleOrDefaultAsync();
                    _userDbContext.UserRoles.Remove(
                        userRoleEntity
                            ?? throw new ArgumentNullException("This should never happen")
                    );
                }
            }

            await _userDbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult<UserDTO>> GetUser(Guid userId)
        {
            var user = await _userDbContext.Users
                .Where(x => x.Id == userId)
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .SingleOrDefaultAsync();
            if (user == null)
            {
                return new OperationResult<UserDTO>(
                    NotFoundException.GenerateDefault<FeastHubUser>(userId)
                );
            }

            return new OperationResult<UserDTO>((UserDTO)user);
        }

        private async Task<OperationResult<FeastHubUser>> GetUserEntity(Guid userId)
        {
            var user = await _userDbContext.Users
                .Where(x => x.Id == userId)
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .SingleOrDefaultAsync();
            if (user == null)
            {
                return new OperationResult<FeastHubUser>(
                    NotFoundException.GenerateDefault<FeastHubUser>(userId)
                );
            }

            return new OperationResult<FeastHubUser>(user);
        }

        public async Task<OperationResult<PagedView<UserDTO>>> GetUsers(UserQueryParameters query)
        {
            var usersQuery = _userDbContext.Users
                .FilterName(query.Name)
                .FilterEmail(query.Email)
                .FilterRoles(query.Roles)
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role);

            var count = await usersQuery.CountAsync();

            if (
                !PagedViewUtilities.PageExists(
                    query.Page,
                    count,
                    _pagedViewOptions.RestaurantPageSize
                )
            )
            {
                return new OperationResult<PagedView<UserDTO>>(
                    new NotFoundException("Requested page does not exist")
                );
            }

            var users = await usersQuery
                .SortUsers(query.SortingParameters)
                .Skip((query.Page - 1) * _pagedViewOptions.UserPageSize)
                .Take(_pagedViewOptions.UserPageSize)
                .ToListAsync();

            var result = new PagedView<UserDTO>
            {
                Items = users.Select(x => (UserDTO)x).ToList(),
                Info = new PageInfo
                {
                    CurrentPage = query.Page,
                    PageCount = PagedViewUtilities.CountPages(
                        count,
                        _pagedViewOptions.UserPageSize
                    ),
                    PageSize = users.Count
                }
            };

            return new OperationResult<PagedView<UserDTO>>(result);
        }
    }
}
