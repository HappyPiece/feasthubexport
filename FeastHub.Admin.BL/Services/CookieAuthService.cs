﻿using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using FeastHub.User.BL.Common;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FeastHub.Admin.BL.Services
{
    public class CookieAuthService : ICookieAuthService
    {
        private readonly FeastHubUserDbContext _dbContext;
        private readonly UserManager<FeastHubUser> _userManager;
        private readonly SignInManager<FeastHubUser> _signInManager;
        public CookieAuthService(FeastHubUserDbContext dbContext, UserManager<FeastHubUser> userManager, SignInManager<FeastHubUser> signInManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public async Task<OperationResult<ClaimsIdentity>> Login(LoginDTO dto)
        {
            var user = await _dbContext.Users
                .Include(x => x.Roles).ThenInclude(x => x.Role)
                .Where(x => x.Email == dto.Email).SingleOrDefaultAsync();
            if (user == null || !(await _userManager.CheckPasswordAsync(user, dto.Password)))
            {
                return new OperationResult<ClaimsIdentity>(new InvalidCredentialsException("Invalid email or password"));
            }
            if (user.IsBanned)
            {
                return new OperationResult<ClaimsIdentity>(new AccessDeniedException("Your account has been suspended. " +
                    "If you think this is a mistake, please contact support"));
            }
            if (!(await _userManager.IsInRoleAsync(user, FeastHubRoleType.Administrator.ToString())
                || await _userManager.IsInRoleAsync(user, FeastHubRoleType.Root.ToString())))
            {
                return new OperationResult<ClaimsIdentity>(new AccessDeniedException("You do not have rights to access this resource"));
            }

            var authProperties = new AuthenticationProperties
            {
                ExpiresUtc = DateTimeOffset.UtcNow.AddDays(2),
                IsPersistent = true
            };
            await _signInManager.SignInWithClaimsAsync(user, authProperties, user.GetClaims());

            return new OperationResult<ClaimsIdentity>(new ClaimsIdentity(user.GetClaims(), "ApplicationCookie", ClaimConfiguration.NameClaimType, ClaimConfiguration.RoleClaimType));
        }

        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
        }
    }
}
