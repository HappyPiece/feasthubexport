﻿using FeastHub.Backend.DAL;
using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FeastHub.Backend.BL.Common;
using Microsoft.EntityFrameworkCore;
using FeastHub.Backend.DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using FeastHub.User.DAL.Models;
using FeastHub.User.DAL;
using Microsoft.Extensions.Options;

namespace FeastHub.Admin.BL.Services
{
    public class RestaurantManagementService : IRestaurantManagementService
    {
        private readonly UserManager<FeastHubUser> _userManager;
        private readonly FeastHubBackendDbContext _dbContext;
        private readonly FeastHubUserDbContext _userDbContext;
        private readonly PagedViewOptions _pagedViewOptions;

        public RestaurantManagementService(
            FeastHubBackendDbContext dbContext,
            UserManager<FeastHubUser> userManager,
            FeastHubUserDbContext userDbContext,
            IOptions<PagedViewOptions> pagedViewOptions
        )
        {
            _dbContext = dbContext;
            _userManager = userManager;
            _userDbContext = userDbContext;
            _pagedViewOptions = pagedViewOptions.Value;
        }

        public async Task<OperationResult<List<UserDTO>>> GetPersonnel(
            Guid id,
            FeastHubRoleType role
        )
        {
            if (role != FeastHubRoleType.Manager && role != FeastHubRoleType.Cook)
            {
                return new OperationResult<List<UserDTO>>(
                    new InvalidOperationException("You dumb or what?")
                );
            }

            var restaurantResult = await GetRestaurantEntity(id);
            if (restaurantResult.IsFailed)
            {
                return new OperationResult<List<UserDTO>>(restaurantResult.Exception!);
            }
            var restaurant = restaurantResult.Value!;

            var user = await GetUserEntity(id);
            if (user == null)
            {
                return new OperationResult<List<UserDTO>>(
                    NotFoundException.GenerateDefault<FeastHubUser>(id)
                );
            }

            var result = new List<UserDTO>();

            if (role == FeastHubRoleType.Cook)
            {
                foreach (var cookId in restaurant.Cooks)
                {
                    var cook = await GetUserEntity(cookId);
                    if (cook.IsSucceeded)
                        result.Add((UserDTO)cook.Value!);
                }
            }
            else if (role == FeastHubRoleType.Manager)
            {
                foreach (var managerId in restaurant.Managers)
                {
                    var manager = await GetUserEntity(managerId);
                    if (manager.IsSucceeded)
                        result.Add((UserDTO)manager.Value!);
                }
            }
            return new OperationResult<List<UserDTO>>(result);
        }

        public async Task<OperationResult> CreateRestaurant(CreateRestaurantDTO dto)
        {
            if (await _dbContext.Restaurants.AnyAsync(x => x.Name == dto.Name))
            {
                return new OperationResult(
                    new AlreadyExistsException($"Restaurant with name {dto.Name} already exists")
                );
            }

            await _dbContext.Restaurants.AddAsync(new Restaurant { Name = dto.Name });
            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> DeleteRestaurant(Guid id)
        {
            if (!await _dbContext.Restaurants.AnyAsync(x => x.Id == id))
            {
                return new OperationResult(NotFoundException.GenerateDefault<Restaurant>(id));
            }

            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();

            _dbContext.Remove(restaurant!);
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> EditPersonnel(
            Guid id,
            Guid userId,
            ActionType actionType,
            FeastHubRoleType role
        )
        {
            if (role != FeastHubRoleType.Manager && role != FeastHubRoleType.Cook)
            {
                return new OperationResult(new InvalidOperationException("You dumb or what?"));
            }

            var restaurantResult = await GetRestaurantEntity(id);
            if (restaurantResult.IsFailed)
            {
                return new OperationResult<RestaurantDTO>(restaurantResult.Exception!);
            }
            var restaurant = restaurantResult.Value!;

            var user = await GetUserEntity(userId);
            if (user.IsFailed)
            {
                return new OperationResult(NotFoundException.GenerateDefault<FeastHubUser>(id));
            }

            if (actionType == ActionType.Add)
            {
                if (role == FeastHubRoleType.Cook)
                {
                    if (restaurant.Cooks.Any(x => x == userId))
                    {
                        return new OperationResult(
                            new AlreadyExistsException(
                                $"User already is a {FeastHubRoleType.Cook} in restaurant '{id}'"
                            )
                        );
                    }
                    restaurant.Cooks.Add(userId);
                }
                else if (role == FeastHubRoleType.Manager)
                {
                    if (restaurant.Managers.Any(x => x == userId))
                    {
                        return new OperationResult(
                            new AlreadyExistsException(
                                $"User already is a {FeastHubRoleType.Manager} in restaurant '{id}'"
                            )
                        );
                    }
                    restaurant.Managers.Add(userId);
                }
            }
            else if (actionType == ActionType.Remove)
            {
                if (role == FeastHubRoleType.Cook)
                {
                    Console.WriteLine(String.Join("; ", restaurant.Cooks));
                    Console.WriteLine(userId);
                    if (!restaurant.Cooks.Any(x => x == userId))
                    {
                        return new OperationResult(
                            new NotFoundException(
                                $"User is not a {FeastHubRoleType.Cook} in restaurant '{id}'"
                            )
                        );
                    }
                    restaurant.Cooks.Remove(userId);
                }
                else if (role == FeastHubRoleType.Manager)
                {
                    if (!restaurant.Managers.Any(x => x == userId))
                    {
                        return new OperationResult(
                            new NotFoundException(
                                $"User is not a {FeastHubRoleType.Manager} in restaurant '{id}'"
                            )
                        );
                    }
                    restaurant.Managers.Remove(userId);
                }
            }

            await _dbContext.SaveChangesAsync();
            return new OperationResult();
        }

        public async Task<OperationResult> EditRestaurant(Guid id, EditRestaurantDTO dto)
        {
            if (!await _dbContext.Restaurants.AnyAsync(x => x.Id == id))
            {
                return new OperationResult(NotFoundException.GenerateDefault<Restaurant>(id));
            }

            if (await _dbContext.Restaurants.AnyAsync(x => x.Name == dto.Name))
            {
                return new OperationResult(
                    new AlreadyExistsException($"Restaurant with name {dto.Name} already exists")
                );
            }

            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == id)
                .SingleOrDefaultAsync();

            restaurant!.Name = dto.Name ?? restaurant.Name;
            await _dbContext.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult<RestaurantDTO>> GetRestaurant(Guid id)
        {
            var restaurantResult = await GetRestaurantEntity(id);

            if (restaurantResult.IsFailed)
            {
                return new OperationResult<RestaurantDTO>(restaurantResult.Exception!);
            }
            return new OperationResult<RestaurantDTO>((RestaurantDTO)restaurantResult.Value!);
        }

        private async Task<OperationResult<Restaurant>> GetRestaurantEntity(Guid id)
        {
            var restaurant = await _dbContext.Restaurants
                .Where(x => x.Id == id)
                .Include(x => x.Menus)
                .ThenInclude(x => x.Dishes)
                .Include(x => x.Categories)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Ratings)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Categories)
                .SingleOrDefaultAsync();

            if (restaurant == null)
            {
                return new OperationResult<Restaurant>(
                    NotFoundException.GenerateDefault<Restaurant>(id)
                );
            }
            return new OperationResult<Restaurant>(restaurant);
        }

        private async Task<OperationResult<FeastHubUser>> GetUserEntity(Guid userId)
        {
            var user = await _userDbContext.Users
                .Where(x => x.Id == userId)
                .Include(x => x.Roles)
                .ThenInclude(x => x.Role)
                .SingleOrDefaultAsync();
            if (user == null)
            {
                return new OperationResult<FeastHubUser>(
                    NotFoundException.GenerateDefault<FeastHubUser>(userId)
                );
            }

            return new OperationResult<FeastHubUser>(user);
        }

        public async Task<OperationResult<PagedView<RestaurantShortDTO>>> GetRestaurantsPaged(
            RestaurantQueryParameters query
        )
        {
            var restaurantsQuery = _dbContext.Restaurants
                .FilterName(query.Name)
                .Include(x => x.Dishes)
                .ThenInclude(x => x.Ratings);

            var count = await restaurantsQuery.CountAsync();

            if (
                !PagedViewUtilities.PageExists(
                    query.Page,
                    count,
                    _pagedViewOptions.RestaurantPageSize
                )
            )
            {
                return new OperationResult<PagedView<RestaurantShortDTO>>(
                    new NotFoundException("Requested page does not exist")
                );
            }

            var restaurants = await restaurantsQuery
                .SortRestaurants(query.SortingParameters)
                .Skip((query.Page - 1) * _pagedViewOptions.RestaurantPageSize)
                .Take(_pagedViewOptions.RestaurantPageSize)
                .ToListAsync();

            var result = new PagedView<RestaurantShortDTO>
            {
                Items = restaurants.Select(x => (RestaurantShortDTO)x).ToList(),
                Info = new PageInfo
                {
                    CurrentPage = query.Page,
                    PageCount = PagedViewUtilities.CountPages(
                        count,
                        _pagedViewOptions.RestaurantPageSize
                    ),
                    PageSize = restaurants.Count
                }
            };

            return new OperationResult<PagedView<RestaurantShortDTO>>(result);
        }
    }
}
