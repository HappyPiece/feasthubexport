﻿using FeastHub.Admin.BL.Services;
using FeastHub.Backend.DAL;
using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.User.BL.Common;
using FeastHub.User.DAL;
using FeastHub.User.DAL.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace FeastHub.Admin.BL
{
    public static class AdminBLExtension
    {
        public static IServiceCollection AddAdminBL(this IServiceCollection services, string userDbConnectionString, string backendDbConnectionString)
        {
            services.AddUserDAL(userDbConnectionString);
            services.AddBackendDAL(backendDbConnectionString);
            services.AddScoped<IUserStore<FeastHubUser>, FeastHubUserStore>();

            services.AddIdentity<FeastHubUser, FeastHubRole>(options =>
            {

            }).AddEntityFrameworkStores<FeastHubUserDbContext>()
            .AddUserManager<FeastHubUserManager>()
            .AddSignInManager<SignInManager<FeastHubUser>>()
            .AddRoleManager<RoleManager<FeastHubRole>>()
            .AddTokenProvider("Default", typeof(FeastHubTokenProvider));

            services.AddScoped<IRestaurantManagementService, RestaurantManagementService>();
            services.AddScoped<IUserManagementService, UserManagementService>();
            services.AddScoped<ICookieAuthService, CookieAuthService>();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password = BasicIdentityOptions.PasswordOptions;
                options.User = BasicIdentityOptions.UserOptions;
            });

            return services;
        }
        public static async Task<WebApplication> UseAdminBL(this WebApplication app, CreateUserDTO? rootOptions)
        {
            await app.UseUserDAL();
            await app.UseBackendDAL();

            await app.EnsureRolesCreated();
            await app.EnsureRootCreated(rootOptions);

            return app;
        }
    }
}
