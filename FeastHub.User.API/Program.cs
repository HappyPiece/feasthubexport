using FeastHub.Common;
using FeastHub.Common.Configurations;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions.ExceptionHandler;
using FeastHub.User.BL;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services
    .BindOptions<AppTokenOptions>(builder.Configuration.GetRequiredSection("Token"))
    .BindOptions<PagedViewOptions>(builder.Configuration.GetSection("PagedView"));

builder.Services
    .AddControllers()
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

builder.Services.AddUserBL(
    dbConnectionString: builder.Configuration
        .GetRequiredSection("UserDb")
        .Get<DbOptions>()!
        .Connection
);

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition(
        FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme.Reference.Id,
        FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme
    );
    options.AddSecurityRequirement(
        new OpenApiSecurityRequirement
        {
            { FeastHub.Common.OpenApiSecurityScheme.JwtSecurityScheme, new List<string>() }
        }
    );

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

builder.Services.AddJwtAuthentication(
    builder.Configuration.GetRequiredSection("Token").Get<AppTokenOptions>()!
);

builder.Services.AddAuthorization();

var app = builder.Build();

await app.UseUserBL(builder.Configuration.GetSection("Root").Get<CreateUserDTO>());
app.UseExceptionInterceptorMiddleware();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
