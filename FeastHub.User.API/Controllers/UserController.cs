﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.DTO.View.QueryParameters;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace FeastHub.User.API.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet, Route("profile"), Authorize]
        public async Task<IActionResult> GetProfile()
        {
            var result = await _userService.GetProfile(ExtractUserGuid(User));
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPatch, Route("profile"), Authorize]
        public async Task<IActionResult> EditProfile([FromBody] EditUserDTO dto)
        {
            var result = await _userService.EditProfile(ExtractUserGuid(User), dto);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        private static Guid ExtractUserGuid(ClaimsPrincipal? principal)
        {
            if (principal.Identity is null)
            {
                throw new ArgumentNullException("User.Identity", "Expected not null User.Identity");
            }
            if (!Guid.TryParse(principal.Identity.Name, out var id))
            {
                throw new BadTokenException(
                    "Expected User.Identity.Name to be valid string representation of Guid"
                );
            }
            return id;
        }

        [HttpPut, Route("change-password")]
        public async Task<IActionResult> ChangePassword([FromBody] EditPasswordDTO dto)
        {
            var result = await _userService.ChangePassword(dto);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
