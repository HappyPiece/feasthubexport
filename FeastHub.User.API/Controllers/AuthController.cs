﻿using FeastHub.Common.Contracts;
using FeastHub.Common.DTO;
using FeastHub.Common.Exceptions;
using FeastHub.Common.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FeastHub.User.API.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [HttpPost, Route("register")]
        public async Task<IActionResult> Register(CreateUserDTO dto)
        {
            var result = await _authService.Register(dto);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("login")]
        public async Task<IActionResult> Login(LoginDTO dto)
        {
            var result = await _authService.Login(dto);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("logout"), Authorize]
        public async Task<IActionResult> Logout()
        {
            var id = GuidUtilities.ExtractGuidFromClaimsPrincipal(User);
            var result = await _authService.Logout(id);
            return result.Evaluate<IActionResult>(
                NoContent,
                OperationResultUtilities.DefaultEvaluator
            );
        }

        [HttpPost, Route("refresh")]
        public async Task<IActionResult> Refresh(TokenPairDTO dto)
        {
            var result = await _authService.Refresh(dto);
            return result.Evaluate<IActionResult>(
                () =>
                {
                    return Ok(result.Value);
                },
                OperationResultUtilities.DefaultEvaluator
            );
        }
    }
}
